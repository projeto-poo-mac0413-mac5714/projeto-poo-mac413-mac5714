mod component;
mod entity;
mod simulation;

pub mod prelude {
    pub use crate::component::*;
    pub use crate::entity::*;
    pub use crate::simulation::*;
    pub use async_trait::async_trait;
}

#[cfg(test)]
mod tests {
    use super::prelude::*;

    struct DummyComponent {}

    impl Component for DummyComponent {
        fn as_any(&mut self) -> &mut dyn std::any::Any {
            self
        }
    }

    #[test]
    fn add_and_remove_entities() {
        let mut simulation = Simulation::new();

        for i in 0..100 {
            let entity = simulation.new_entity();
            simulation.add_entity(entity).unwrap();

            assert!(simulation.get_entity(i + 1).is_ok());
        }

        for i in 0..100 {
            simulation.remove_entity(i).unwrap();

            assert!(simulation.get_entity(i + 1).is_err());
        }
    }
}
