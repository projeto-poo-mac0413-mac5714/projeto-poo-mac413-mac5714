use std::{any::TypeId, cmp::Ordering, collections::HashMap, mem::replace};

use crate::{component::Component, prelude::ComponentCall, simulation::Simulation};

// type Rc<RefCell<>>
pub type EntityID = u32;

pub struct Entity {
    id: EntityID,
    components: HashMap<TypeId, Box<dyn Component + 'static>>,
    sequence: i32,
    remove_queue: Vec<TypeId>,
}

impl Entity {
    pub fn add_component<T: Component>(&mut self, component: T) -> Result<(), &'static str> {
        let type_id = component.type_id();

        if self.components.contains_key(&type_id) {
            return Err("Entity already has this type of component");
        }

        self.components.insert(type_id, Box::new(component));

        Ok(())
    }

    pub fn get_component<T: Component + Sized + 'static>(
        &mut self,
    ) -> Result<&mut T, &'static str> {
        let type_id = TypeId::of::<T>();

        let component = match self.components.get_mut(&type_id) {
            Some(component) => component,
            None => return Err("Entity doesn't have this type of component"),
        };

        Ok(component.as_mut().as_any().downcast_mut().unwrap())
    }

    pub fn remove_component<T: Component>(&mut self) -> Result<(), &'static str> {
        let type_id = TypeId::of::<T>();

        match self.components.remove(&type_id) {
            Some(component) => component,
            None => return Err("Entity doesn't have this type of component"),
        };

        Ok(())
    }

    pub fn get_id(&self) -> EntityID {
        self.id
    }

    /// Set update priority
    pub fn set_sequence(mut self, sequence: i32) -> Self {
        self.sequence = sequence;
        self
    }

    /// Set update priority
    pub fn sequence(&mut self, sequence: i32) {
        self.sequence = sequence;
    }

    pub(super) async fn call_component(
        mut self,
        component_call: ComponentCall,
        simulation: &mut Simulation,
    ) -> Entity {
        let mut id_values: Vec<(&TypeId, &Box<dyn Component>)> = self.components.iter().collect();
        id_values.sort_by(|a, b| {
            a.1.as_ref()
                .sequence()
                .partial_cmp(&b.1.sequence())
                .unwrap()
        });

        let ids: Vec<TypeId> = id_values.iter().map(|x| *x.0).collect();

        for id in ids {
            let Some(mut component) = self.components.remove(&id) else {
                continue;
            };
            if !(simulation.is_paused() && component.pauseable()) {
                component.call(component_call, simulation, &mut self).await;
            }
            self.components.insert(id, component);
        }

        let remove_queue = replace(&mut self.remove_queue, Vec::new());

        for type_id in remove_queue.into_iter() {
            let _ = self.components.remove(&type_id);
        }

        self.remove_queue = Vec::new();

        self
    }

    pub(super) async fn update(self, simulation: &mut Simulation) -> Entity {
        self.call_component(ComponentCall::Update, simulation).await
    }
    pub(super) async fn init(self, simulation: &mut Simulation) -> Entity {
        self.call_component(ComponentCall::Init, simulation).await
    }

    pub(super) fn new(id: EntityID) -> Entity {
        Entity {
            id,
            components: HashMap::new(),
            sequence: 0,
            remove_queue: vec![],
        }
    }

    pub fn enqueue_remove<C: Component>(&mut self) {
        self.remove_queue.push(TypeId::of::<C>());
    }
}

impl PartialEq for Entity {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.sequence == other.sequence
    }
}

impl Eq for Entity {}

impl PartialOrd for Entity {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.sequence.partial_cmp(&other.sequence) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        self.id.partial_cmp(&other.id)
    }
}
impl Ord for Entity {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.sequence.cmp(&other.sequence)
    }
}
