use std::{collections::VecDeque, mem::replace};

use entity::EntityID;

use crate::{
    component::Component,
    entity::{self, Entity},
};

pub struct Simulation {
    entities: VecDeque<Entity>,
    available_id: u32,
    remove_queue: Vec<EntityID>,
    paused: bool,
}

impl Simulation {
    pub fn new_entity(&mut self) -> Entity {
        self.available_id += 1;
        Entity::new(self.available_id - 1)
    }

    pub fn set_paused(&mut self, paused: bool) {
        self.paused = paused;
    }

    pub fn is_paused(&self) -> bool {
        self.paused
    }

    pub fn add_entity(&mut self, entity: Entity) -> Result<(), &'static str> {
        if self.entities.iter().any(|e| e.get_id() == entity.get_id()) {
            return Err("Entity already added");
        }

        self.entities.push_back(entity);

        Ok(())
    }

    pub fn remove_entity(&mut self, entity_id: EntityID) -> Result<(), &'static str> {
        let i = self
            .entities
            .iter()
            .position(|e| e.get_id() == entity_id)
            .ok_or("Entity not Found")?;

        self.entities.remove(i);

        Ok(())
    }

    /// Remove the entity from the game after an update
    pub fn enqueue_remove(&mut self, entity_id: EntityID) {
        self.remove_queue.push(entity_id);
    }

    pub fn get_entity(&mut self, entity_id: EntityID) -> Result<&mut Entity, &'static str> {
        let i = self
            .entities
            .iter()
            .position(|e| e.get_id() == entity_id)
            .ok_or("Entity not Found")?;

        Ok(self.entities.get_mut(i).ok_or("Entity not found")?)
    }

    pub fn query<T: Component>(&mut self) -> VecDeque<&mut Entity> {
        self.entities
            .iter_mut()
            .filter_map(|e| {
                if e.get_component::<T>().is_ok() {
                    Some(e)
                } else {
                    None
                }
            })
            .collect()
    }

    pub async fn update(mut self) -> Simulation {
        self.entities.make_contiguous().sort();
        for _ in 0..self.entities.len() {
            let entity = self.entities.pop_front().unwrap();
            let entity = entity.update(&mut self).await;
            if !self.remove_queue.contains(&entity.get_id()) {
                self.entities.push_back(entity);
            }
        }

        let remove_queue = replace(&mut self.remove_queue, Vec::new());

        for entity_id in remove_queue.into_iter() {
            let _ = self.remove_entity(entity_id);
        }

        self.remove_queue = Vec::new();

        self
    }

    pub async fn init(mut self) -> Simulation {
        for _ in 0..self.entities.len() {
            let entity = self.entities.pop_front().unwrap();
            let entity = entity.init(&mut self).await;
            self.entities.push_back(entity);
        }

        self
    }

    pub fn new() -> Simulation {
        Simulation {
            entities: VecDeque::new(),
            available_id: 1,
            remove_queue: Vec::new(),
            paused: false,
        }
    }

    pub fn register_resource<C: Component>(&mut self, resource: C) -> Result<(), &str> {
        if !self.query::<C>().is_empty() {
            return Err("Resource already registered");
        }

        let mut entity = self.new_entity();
        entity.add_component(resource).unwrap();
        self.add_entity(entity)
    }

    pub fn get_resource<C: Component>(&mut self) -> Option<&mut C> {
        let mut entities = self.query::<C>();
        entities
            .pop_back()
            .and_then(|e| Some(e.get_component().unwrap()))
    }
}
