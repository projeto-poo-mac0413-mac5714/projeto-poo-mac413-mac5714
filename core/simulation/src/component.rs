use std::{any::Any, io};

use async_trait::async_trait;

use crate::{prelude::Entity, simulation::Simulation};

#[derive(Clone, Copy)]
pub(super) enum ComponentCall {
    Update,
    Init,
}

#[async_trait(?Send)]
pub trait Component: Any {
    fn as_any(&mut self) -> &mut dyn Any;

    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        Ok(())
    }

    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn sequence(&self) -> i32 {
        0
    }

    fn pauseable(&self) -> bool {
        true
    }
}

impl dyn Component {
    pub(super) async fn call(
        &mut self,
        component_call: ComponentCall,
        simulation: &mut Simulation,
        entity: &mut Entity,
    ) {
        match component_call {
            ComponentCall::Update => self.update(entity, simulation).await.unwrap(),
            ComponentCall::Init => self.init(entity, simulation).await.unwrap(),
        }
    }
}
