mod game;

pub mod prelude {
    pub use crate::game::*;
    pub use simulation::prelude::*;
}
