use std::mem::replace;

use simulation::prelude::Simulation;

pub struct Game {
    simulation: Simulation,
    game_loop: Box<dyn GameLoop>,
}

pub trait GameLoop {
    fn run(&mut self, simulation: Simulation);
}

impl Game {
    pub fn new(simulation: Simulation, game_loop: Box<dyn GameLoop>) -> Game {
        Game {
            simulation,
            game_loop,
        }
    }

    pub fn run(&mut self) {
        let simulation = replace(&mut self.simulation, Simulation::new());
        self.game_loop.run(simulation);
    }
}
