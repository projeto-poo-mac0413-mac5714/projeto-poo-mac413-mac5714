use mq_game::prelude::*;
use std::{collections::HashMap, f32::consts::PI, io};

mod bullet_strategy;
mod shooter;

pub use bullet_strategy::*;
pub use shooter::*;

const DIST: f32 = 37.0;
const DEFAULT_COLLISIONS: i32 = 5;

pub type BulletType = i32;

// MAP to make the player calls easier
pub struct BulletTypes<'a> {
    types: HashMap<&'a str, BulletType>,
}

impl BulletTypes<'_> {
    pub fn new() -> Self {
        BulletTypes {
            types: vec![("default", 1), ("special", 2)].into_iter().collect(),
        }
    }

    pub fn get(&self, bullet_type: &str) -> BulletType {
        *self.types.get(bullet_type).unwrap()
    }
}

// contexto
pub struct Bullet {
    pub bullet_strategy: Box<dyn BulletStrategy>,
    pub collisions: i32,
}

impl Bullet {
    // pub fn get_damage(&self) -> f32 {
    //     self.bullet_strategy.get_damage()
    // }

    pub fn new_entity(
        simulation: &mut Simulation,
        bullet_strategy: Box<dyn BulletStrategy>,
        position: Vec2,
        rotation: f32,
    ) -> Entity {
        let mut entity = simulation.new_entity();

        let velocity = bullet_strategy.get_velocity();

        entity
            .add_component(Sprite::new(bullet_strategy.get_asset_path().to_string()).set_layer(2))
            .unwrap();

        let mut transf = Transform::new();
        transf.scale = Vec2::new(20.0, 20.0);
        transf.position = position - Vec2::from_angle(rotation - PI / 2.0) * DIST;
        transf.rotation = rotation;
        entity.add_component(transf).unwrap();

        let mut kinematic = Kinematic::new();
        kinematic.velocity = Vec2::from_angle(rotation + PI / 2.0) * velocity;
        entity
            .add_component(kinematic.set_collision_mode(CollisionMode::Reflect))
            .unwrap();

        entity
            .add_component(
                CollisionBox::new(CollisionShape::Circle { radius: 10. }, 2, 3).set_area_masks(1),
            )
            .unwrap();

        entity
            .add_component(Self {
                bullet_strategy,
                collisions: DEFAULT_COLLISIONS,
            })
            .unwrap();

        entity
    }

    fn check_collision(&mut self, entity: &mut Entity, simulation: &mut Simulation) {
        let collision = entity.get_component::<CollisionBox>().unwrap();

        let colliders: Vec<EntityID> = collision.get_colliders();

        if !colliders.is_empty() {
            self.collisions -= 1;
        }
        if self.collisions == 0 {
            simulation.enqueue_remove(entity.get_id());
        }
    }
}
#[async_trait(?Send)]
impl Component for Bullet {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        self.check_collision(_entity, _simulation);
        Ok(())
    }
}
