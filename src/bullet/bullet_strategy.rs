pub trait BulletStrategy {
    fn get_velocity(&self) -> f32;
    fn get_damage(&self) -> f32;
    fn get_asset_path(&self) -> &'static str;
}

impl BulletStrategy for DefaultBullet {
    fn get_damage(&self) -> f32 {
        15.0
    }
    fn get_velocity(&self) -> f32 {
        5000.0
    }
    fn get_asset_path(&self) -> &'static str {
        "assets/bullet.png"
    }
}

pub struct DefaultBullet {}

pub struct SpecialBullet {}

impl BulletStrategy for SpecialBullet {
    fn get_damage(&self) -> f32 {
        45.0
    }
    fn get_velocity(&self) -> f32 {
        6500.0
    }
    fn get_asset_path(&self) -> &'static str {
        "assets/special-bullet.png"
    }
}
