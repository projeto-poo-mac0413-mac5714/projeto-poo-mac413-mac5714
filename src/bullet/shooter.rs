use super::{Bullet, BulletType, BulletTypes, DefaultBullet, SpecialBullet};
use mq_game::prelude::*;

pub struct Shooter {
    pub bullet_type: BulletType,
}

#[async_trait(?Send)]
impl Component for Shooter {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    fn sequence(&self) -> i32 {
        9
    }
}

impl Shooter {
    pub fn set_bullet_type(&mut self, bullet_type: &str) {
        self.bullet_type = BulletTypes::new().get(bullet_type);
    }

    pub fn shoot(&self, simulation: &mut Simulation, transform: Transform) {
        if self.bullet_type == BulletTypes::new().get("default") {
            let bullet = Bullet::new_entity(
                simulation,
                Box::new(DefaultBullet {}),
                transform.position,
                transform.rotation,
            );
            simulation.add_entity(bullet).unwrap();
        } else if self.bullet_type == BulletTypes::new().get("special") {
            let bullet = Bullet::new_entity(
                simulation,
                Box::new(SpecialBullet {}),
                transform.position,
                transform.rotation,
            );
            simulation.add_entity(bullet).unwrap();
        } else {
            println!("Bullet type unrecognized");
        }
    }
}
