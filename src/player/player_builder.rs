use commands::CommandManager;
use mq_game::prelude::*;
use std::{
    collections::HashMap,
    fs::{self, File},
    io::BufReader,
    mem,
};

use crate::{commands::*, Args};

use super::Player;

const SERVER_PORT: u32 = 9091;
const CLIENT_PORT: u32 = 9092;

const DEFAULT_BINDINGS: &str = r#"
{ 
    "player_button_mapping": {
        "1": { 
            "1": { "key": "W", "to_hold": true },
            "2": { "key": "S", "to_hold": true },
            "3": { "key": "A", "to_hold": true },
            "4": { "key": "D", "to_hold": true },
            "5": { "key": "Space", "to_hold": false }
        },
        "2": {
            "1": { "key": "Up", "to_hold": true },
            "2": { "key": "Down", "to_hold": true },
            "3": { "key": "Left", "to_hold": true },
            "4": { "key": "Right", "to_hold": true },
            "5": { "key": "Z", "to_hold": false }
        }
    }
}
"#;

pub fn is_online_session(config: Args) -> bool {
    config.host || config.connect.is_some()
}

pub fn build_players(simulation: &mut Simulation, config: Args) {
    if let Some(replay_file) = config.clone().load_replay {
        insert_reader_factories(simulation, replay_file);
    } else {
        insert_input_factories(simulation, config.clone());
    }

    if is_online_session(config.clone()) {
        connect_players(simulation, config.clone());
    }

    if let Some(replay_file) = config.clone().save_replay {
        insert_recorder_factories(simulation, replay_file);
    }

    // TOOO: implement replay saving
    // if let Some(_replay_file) = config.save_replay {}
}

type BindingJSON = HashMap<String, HashMap<u8, HashMap<u8, Key>>>;

fn insert_input_factories(simulation: &mut Simulation, config: Args) {
    let bindings = if let Ok(bindings_from_file) = fs::read_to_string("bindings.json") {
        bindings_from_file
    } else {
        String::from(DEFAULT_BINDINGS)
    };

    let config_file = serde_json::from_str::<BindingJSON>(bindings.as_str()).unwrap();

    let bindings = config_file.get("player_button_mapping").unwrap();

    let players = simulation
        .query::<Player>()
        .into_iter()
        .map(|e| e.get_id())
        .collect::<Vec<_>>();

    let mut to_remove = vec![];
    set_players_local_input(players, simulation, config, bindings, &mut to_remove);

    for id in to_remove.iter() {
        let _ = simulation.remove_entity(*id);
    }
}

fn set_players_local_input(
    players: Vec<u32>,
    simulation: &mut Simulation,
    config: Args,
    bindings: &HashMap<u8, HashMap<u8, Key>>,
    to_remove: &mut Vec<u32>,
) {
    for player_entity_id in players {
        let player = simulation.get_entity(player_entity_id).unwrap();
        let player_id = player.get_component::<Player>().unwrap().get_player_id();
        let binding_id = if config.connect.is_some() && player_id == 2 {
            1
        } else {
            player_id
        };

        let binding = if let Some(b) = bindings.get(&binding_id) {
            b
        } else {
            to_remove.push(player.get_id());
            continue;
        };

        // Vec<(Key, Box<dyn Command>)>

        let mapping = binding
            .iter()
            .map(|(command_id, key)| {
                (
                    *key,
                    simulation
                        .get_resource::<CommandManager>()
                        .unwrap()
                        .command_from_id(*command_id)
                        .unwrap(),
                )
            })
            .collect();

        let player = simulation.get_entity(player_entity_id).unwrap();
        player.get_component::<Player>().unwrap().factory =
            Box::new(LocalInputCommandFactory::new(mapping).unwrap());
    }
}

fn connect_players(simulation: &mut Simulation, config: Args) {
    let mut network_manager_entity = NetworkManager::new_entity(simulation);
    let players = simulation.query::<Player>();

    let network_manager = network_manager_entity
        .get_component::<NetworkManager>()
        .unwrap();

    let peer_id = if config.host { 1 } else { 2 };

    let ip = if config.host {
        "127.0.0.1".into()
    } else {
        config.connect.unwrap()
    };

    for player in players {
        let player_entity_id = player.get_id();
        let player_id = player.get_component::<Player>().unwrap().get_player_id();

        if peer_id == player_id {
            player.sequence(10);
        }

        let player = player.get_component::<Player>().unwrap();

        if peer_id == player_id {
            let peer = if player_id == 1 {
                network_manager.host(SERVER_PORT, PeerType::Sender)
            } else {
                network_manager.connect(ip.clone(), CLIENT_PORT, PeerType::Sender)
            };

            let factory = mem::replace(&mut player.factory, Box::new(DummyCommandFactory));
            player.factory = Box::new(NetworkSendCommandFactory::new(factory, peer));
        } else {
            let peer = if player_id == 1 {
                network_manager.connect(ip.clone(), SERVER_PORT, PeerType::Receiver)
            } else {
                network_manager.host(CLIENT_PORT, PeerType::Receiver)
            };
            player.factory = Box::new(NetworkReceiveCommandFactory::new(peer, player_entity_id));
        }
    }
    simulation.add_entity(network_manager_entity).unwrap();
}

fn insert_reader_factories(simulation: &mut Simulation, filename: String) {
    let players = simulation.query::<Player>();

    let mut to_remove = vec![];

    for player in players {
        let entity_id = player.get_id();
        let player = player.get_component::<Player>().unwrap();

        if player.get_player_id() != 1 && player.get_player_id() != 2 {
            to_remove.push(entity_id);
        }

        let file = File::open(filename.clone()).unwrap();
        player.factory = Box::new(ReadReplayCommandFactory {
            player_id: player.get_player_id(),
            file: BufReader::new(file),
        });
    }

    for id in to_remove.iter() {
        let _ = simulation.remove_entity(*id);
    }
}

fn insert_recorder_factories(simulation: &mut Simulation, filename: String) {
    let players = simulation.query::<Player>();

    let file = File::create(filename.clone()).unwrap();
    for player in players {
        let player = player.get_component::<Player>().unwrap();

        let factory = mem::replace(&mut player.factory, Box::new(DummyCommandFactory));

        player.factory = Box::new(WriteReplayCommandFactory {
            player_id: player.get_player_id(),
            factory,
            file: file.try_clone().unwrap(),
        });
    }
}
