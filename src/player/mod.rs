use commands::CommandFactory;
use mq_game::prelude::*;

use std::io;

use crate::bullet::{Bullet, BulletTypes, Shooter};

mod event;
mod healthbar;
mod player_builder;

pub use event::*;
pub use healthbar::*;
pub use player_builder::*;

pub type PlayerID = u8;

pub struct Player {
    factory: Box<dyn CommandFactory>,
    id: PlayerID,
}

impl Player {
    pub fn get_player_id(&self) -> u8 {
        self.id
    }

    pub fn new(
        simulation: &mut Simulation,
        position: Vec2,
        factory: impl CommandFactory + 'static,
        sprite: SpriteParameter,
        id: u8,
    ) -> Entity {
        let mut entity: Entity = simulation.new_entity();
        let player_sprite = match sprite {
            SpriteParameter::PathToSprite(s) => Sprite::new(s),
            SpriteParameter::LoadedSprite(s) => s,
        };

        entity.add_component(player_sprite.set_layer(2)).unwrap();

        let mut transf = Transform::new();
        transf.position = position;
        transf.scale = Vec2::new(50.0, 50.0);

        entity.add_component(transf).unwrap();
        entity.add_component(Kinematic::new()).unwrap();
        entity.add_component(Bar::new()).unwrap();
        entity.add_component(HealthBar::new()).unwrap();

        entity
            .add_component(
                CollisionBox::new(CollisionShape::Circle { radius: 24.0 }, 1, 1).set_area_masks(2),
            )
            .unwrap();

        entity
            .add_component(Shooter {
                bullet_type: BulletTypes::new().get("default"),
            })
            .unwrap();

        entity
            .add_component(Player {
                factory: Box::new(factory),
                id,
            })
            .unwrap();

        entity.set_sequence(12)
    }
}

#[async_trait(?Send)]
impl Component for Player {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let _transform = *entity.get_component::<Transform>().unwrap();
        let kinematic = entity.get_component::<Kinematic>().unwrap();

        kinematic.angular_velocity = 0.0;
        kinematic.velocity = Vec2::ZERO;

        let commands = self.factory.generate_commands(simulation).await;

        for command in commands {
            command.execute(entity, simulation);
        }

        let transform = *entity.get_component::<Transform>().unwrap();
        let kinematic = entity.get_component::<Kinematic>().unwrap();

        kinematic.velocity = kinematic
            .velocity
            .rotate(Vec2::from_angle(transform.rotation));
        self.check_collision(entity, simulation);

        Ok(())
    }

    fn sequence(&self) -> i32 {
        9
    }
}

impl Player {
    fn update_health(&self, entity: &mut Entity, simulation: &mut Simulation, damage: f32) {
        // update the health bar
        let health_bar = entity.get_component::<HealthBar>().unwrap();
        health_bar.update_health(damage);

        // we check if the player is dead
        if health_bar.get_health() <= 0.0 {
            let _player_id = entity.get_id();
            let player_position = entity.get_component::<Transform>().unwrap().position;

            Self::destroy(entity.get_id(), simulation);
            add_explosion(simulation, player_position, 100.);
        }
    }

    fn check_collision(&self, entity: &mut Entity, simulation: &mut Simulation) {
        let collision = entity.get_component::<CollisionBox>().unwrap();

        let colliders: Vec<EntityID> = collision.get_colliders();

        for collider in colliders {
            let Ok(collider) = simulation.get_entity(collider) else {
                continue;
            };
            let collider_id = collider.get_id();

            let collider_transform = *collider.get_component::<Transform>().unwrap();

            // if the player got hitten by a bullet
            if collider.get_component::<Bullet>().is_ok() {
                let bullet = collider.get_component::<Bullet>().unwrap();
                let damage = bullet.bullet_strategy.get_damage();
                self.update_health(entity, simulation, damage);
                add_explosion(simulation, collider_transform.position, 50.);

                // remove the bullet from the game
                simulation.enqueue_remove(collider_id);
            }
        }
    }

    pub fn destroy(entity_id: EntityID, simulation: &mut Simulation) {
        let event_emitter = simulation.get_resource::<EventManager>().unwrap();
        event_emitter.emit_event("destroyed".into(), entity_id);
        simulation.enqueue_remove(entity_id);
    }
}

pub fn add_explosion(simulation: &mut Simulation, position: Vec2, size: f32) {
    let mut explosion: Entity = simulation.new_entity().set_sequence(1);

    // explosion transform
    let mut transf = Transform::new();
    transf.position = position;
    transf.scale = Vec2::new(size, size);
    explosion.add_component(transf).unwrap();
    explosion
        .add_component(AnimatedSprite::new().set_frame_rate(0.8).repeat(false))
        .unwrap();
    explosion
        .add_component(
            Sprite::new("assets/effects/explosion.png".to_string())
                .set_reverse_frames(true)
                .crop(4, 4),
        )
        .unwrap();

    simulation.add_entity(explosion).unwrap();
}
