use std::io;

use mq_game::prelude::*;

#[derive(Default)]
pub struct EventManager {
    emitted_events: Vec<(String, EntityID)>,
    emitted_events_buffer: Vec<(String, EntityID)>,
}

impl EventManager {
    pub fn is_event_emitted(&self, event: String, entity_id: EntityID) -> bool {
        self.emitted_events.contains(&(event.clone(), entity_id))
            || self.emitted_events_buffer.contains(&(event, entity_id))
    }

    pub fn emit_event(&mut self, event: String, entity_id: EntityID) {
        self.emitted_events.push((event, entity_id))
    }

    pub fn register(simulation: &mut Simulation) -> Result<(), &str> {
        simulation.register_resource(EventManager::default())
    }
}

#[async_trait(?Send)]
impl Component for EventManager {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        let buffer = std::mem::take(&mut self.emitted_events);
        self.emitted_events_buffer = buffer;

        Ok(())
    }
}
