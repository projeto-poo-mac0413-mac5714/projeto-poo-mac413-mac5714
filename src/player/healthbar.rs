use std::io;

use mq_game::prelude::*;

pub struct HealthBar {
    pub health: f32,
}

impl HealthBar {
    pub fn new() -> HealthBar {
        HealthBar { health: 100.0 }
    }

    pub fn get_health(&self) -> f32 {
        self.health
    }

    pub fn update_health(&mut self, damage: f32) {
        self.health -= damage;
    }
}

#[async_trait(?Send)]
impl Component for HealthBar {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        let bar = entity.get_component::<Bar>().unwrap();
        bar.value = self.health;
        Ok(())
    }

    fn sequence(&self) -> i32 {
        8
    }
}
