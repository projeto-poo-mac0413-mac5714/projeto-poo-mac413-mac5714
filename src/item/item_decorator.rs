use mq_game::prelude::*;
use std::io;

use crate::bullet::{BulletTypes, Shooter};
use crate::item::ACTIVE_FRAMES;
use crate::player::HealthBar;

const SPEED_FACTOR: f32 = 2.0;

pub trait PlayerDecorator {
    fn apply(&mut self, entity: &mut Entity, simulation: &mut Simulation);
    fn activate(&mut self);
    fn deactivate(&mut self);
    fn reset(&mut self);
}

pub struct SpeedDecorator {
    frames_active: i32,
    active: bool,
    expired: bool,
}

impl SpeedDecorator {
    pub fn new() -> SpeedDecorator {
        SpeedDecorator {
            frames_active: 0,
            active: false,
            expired: false,
        }
    }

    pub fn activate_effect(&mut self) {
        self.activate();
    }

    pub fn reset_effect(&mut self) {
        self.reset();
    }

    pub fn check_expire(&self) -> bool {
        self.frames_active > ACTIVE_FRAMES
    }
}
impl PlayerDecorator for SpeedDecorator {
    fn apply(&mut self, entity: &mut Entity, _simulation: &mut Simulation) {
        if self.active {
            let kinematic = entity.get_component::<Kinematic>().unwrap();
            kinematic.angular_velocity *= SPEED_FACTOR;
            kinematic.velocity *= SPEED_FACTOR;

            self.frames_active += 1;
        }

        if self.check_expire() {
            self.deactivate();
        }
    }

    fn activate(&mut self) {
        self.active = true;
    }

    fn deactivate(&mut self) {
        self.active = false;
        self.frames_active = 0;
        self.expired = true;
    }

    fn reset(&mut self) {
        self.active = true;
        self.frames_active = 0;
        self.expired = false;
    }
}

#[async_trait(?Send)]
impl Component for SpeedDecorator {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        self.apply(entity, simulation);

        if self.expired {
            let _ = entity.enqueue_remove::<SpeedDecorator>();
        }
        Ok(())
    }

    fn sequence(&self) -> i32 {
        100
    }
}

pub struct HealingDecorator {
    frames_active: i32,
    active: bool,
    expired: bool,
}

impl HealingDecorator {
    pub fn new() -> HealingDecorator {
        HealingDecorator {
            frames_active: 0,
            active: false,
            expired: false,
        }
    }

    pub fn activate_effect(&mut self) {
        self.activate();
    }

    pub fn reset_effect(&mut self) {
        self.reset();
    }

    pub fn check_expire(&self) -> bool {
        self.frames_active > ACTIVE_FRAMES
    }
}

impl PlayerDecorator for HealingDecorator {
    fn apply(&mut self, entity: &mut Entity, _simulation: &mut Simulation) {
        if self.active {
            let health_bar = entity.get_component::<HealthBar>().unwrap();
            let cur_health = health_bar.get_health();

            if cur_health < 100.0 {
                let missing_health = 100.0 - cur_health;

                let mut update = -2.0;

                if missing_health < update {
                    update = missing_health;
                }

                health_bar.update_health(update);
            }

            self.frames_active += 1;
        }

        if self.check_expire() {
            self.deactivate();
        }
    }

    fn activate(&mut self) {
        self.active = true;
    }

    fn deactivate(&mut self) {
        self.active = false;
        self.frames_active = 0;
        self.expired = true;
    }

    fn reset(&mut self) {
        self.active = true;
        self.frames_active = 0;
        self.expired = false;
    }
}

#[async_trait(?Send)]
impl Component for HealingDecorator {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        self.apply(entity, simulation);

        if self.expired {
            let _ = entity.enqueue_remove::<HealingDecorator>();
        }

        Ok(())
    }

    fn sequence(&self) -> i32 {
        100
    }
}

pub struct ShooterDecorator {
    frames_active: i32,
    active: bool,
    expired: bool,
}

impl ShooterDecorator {
    pub fn new() -> ShooterDecorator {
        ShooterDecorator {
            frames_active: 0,
            active: false,
            expired: false,
        }
    }

    pub fn activate_effect(&mut self) {
        self.activate();
    }

    pub fn reset_effect(&mut self) {
        self.reset();
    }

    pub fn check_expire(&self) -> bool {
        self.frames_active > ACTIVE_FRAMES
    }
}

impl PlayerDecorator for ShooterDecorator {
    fn apply(&mut self, entity: &mut Entity, _simulation: &mut Simulation) {
        if self.active {
            let shooter = entity.get_component::<Shooter>().unwrap();

            let bullet_types = BulletTypes::new();

            if shooter.bullet_type == bullet_types.get("default") {
                shooter.set_bullet_type("special");
            }
            self.frames_active += 1;
        }

        if self.check_expire() {
            self.deactivate();
        }
    }

    fn activate(&mut self) {
        self.active = true;
    }

    fn deactivate(&mut self) {
        self.active = false;
        self.frames_active = 0;
        self.expired = true;
    }

    fn reset(&mut self) {
        self.active = true;
        self.frames_active = 0;
        self.expired = false;
    }
}

#[async_trait(?Send)]
impl Component for ShooterDecorator {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        self.apply(entity, simulation);

        if self.expired {
            let shooter = entity.get_component::<Shooter>().unwrap();
            shooter.set_bullet_type("default");

            let _ = entity.enqueue_remove::<ShooterDecorator>();
        }

        Ok(())
    }

    fn sequence(&self) -> i32 {
        100
    }
}
