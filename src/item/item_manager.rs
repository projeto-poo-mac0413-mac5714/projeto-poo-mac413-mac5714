use mq_game::prelude::*;
use tinyrand::{RandRange, Seeded, StdRand};

use std::io;

use crate::item::Item;

pub struct ItemManager {
    frame: i32,
    update_frame: i32,
    game_width: i32,
    game_height: i32,
    rng: StdRand,
}

pub const SPAWM_PROB: f32 = 0.73;

impl ItemManager {
    pub fn new(width: i32, height: i32) -> ItemManager {
        ItemManager {
            frame: 0,
            update_frame: 150,
            game_width: width,
            game_height: height,
            rng: StdRand::seed(91929102),
        }
    }
    pub fn new_entity(simulation: &mut Simulation, width: i32, height: i32) -> Entity {
        let mut entity = simulation.new_entity().set_sequence(100);
        let _ = entity.add_component(ItemManager::new(width, height));
        entity
    }

    pub fn register(simulation: &mut Simulation, width: i32, height: i32) -> Result<(), &str> {
        let item_manager = ItemManager::new_entity(simulation, width, height);
        simulation.add_entity(item_manager.set_sequence(-10))
    }
}

#[async_trait(?Send)]
impl Component for ItemManager {
    async fn init(&mut self, _entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {

        // Loading textures here to prevent flickering on web builds 
        
        let texture_manager = simulation.get_resource::<TextureManager>().unwrap();
        texture_manager.load_texture("assets/wrench.png".to_string()).await;
        texture_manager.load_texture("assets/effects/disintegrate_32.png".to_string()).await;
        texture_manager.load_texture("assets/effects/poof_32.png".to_string()).await;
        texture_manager.load_texture("assets/bullet.png".to_string()).await;
        texture_manager.load_texture("assets/special-bullet.png".to_string()).await;
        texture_manager.load_texture("assets/explosions/explosion/explosion.png".to_string()).await;

        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        simulation: &mut Simulation,
    ) -> io::Result<()> {
        if self.frame % self.update_frame == 0
            && (self.rng.next_range(0..1000u32) as f32) / 1000.0 <= SPAWM_PROB
        {
            let x = (self.game_width / 4) as f32
                + (self.rng.next_range(0..1000u32) as f32) / 1000.0 * (self.game_width / 2) as f32;
            let y = (self.game_height / 4) as f32
                + (self.rng.next_range(0..1000u32) as f32) / 1000.0 * (self.game_height / 2) as f32;
            let position = Vec2 { x, y };
            let item = Item::new(
                simulation,
                position,
                StdRand::seed(self.rng.next_range(0..100000u64)),
            );
            let _ = simulation.add_entity(item);
            add_spawn_effect(simulation, position, 100.);
        }

        self.frame += 1;
        Ok(())
    }
}

fn add_spawn_effect(simulation: &mut Simulation, position: Vec2, size: f32) {
    let mut spawn: Entity = simulation.new_entity().set_sequence(1);

    // spawn transform
    let mut transf = Transform::new();
    transf.position = position;
    transf.scale = Vec2::new(size, size);
    spawn.add_component(transf).unwrap();
    spawn
        .add_component(AnimatedSprite::new().set_frame_rate(0.25).repeat(false))
        .unwrap();
    spawn
        .add_component(
            Sprite::new("assets/effects/spawn.png".to_string())
                .set_reverse_frames(false)
                .crop(1, 8)
                .set_layer(0),
        )
        .unwrap();

    simulation.add_entity(spawn).unwrap();
}
