use mq_game::prelude::*;
use tinyrand::{RandRange, StdRand};

use crate::player::Player;
use std::io;

mod item_decorator;
mod item_manager;
mod item_strategy;

pub use item_manager::*;
pub use item_strategy::*;

const N_EFFECTS: i32 = 3;
const ACTIVE_FRAMES: i32 = 100;
const EXPIRATION_FRAMES: i32 = 3000;

fn generate_item_effect(mut rng: StdRand) -> Box<dyn ItemEffect> {
    const SPEED: i32 = 0;
    const HEAL: i32 = 1;
    const SPECIALBULLET: i32 = 2;

    let effect_i32 = (rng.next_range(0..1000u32) as i32) % (N_EFFECTS + 1);

    let effect: Box<dyn ItemEffect> = match effect_i32 {
        SPEED => Box::new(SpeedEffect {}),
        HEAL => Box::new(HealingEffect {}),
        SPECIALBULLET => Box::new(SpecialBulletEffect {}),
        _ => Box::new(DummyEffect {}),
    };

    effect
}

pub struct Item {
    pub effect: Box<dyn ItemEffect>,
    pub active_frames: i32,
}

impl Item {
    pub fn new(simulation: &mut Simulation, position: Vec2, rng: StdRand) -> Entity {
        let effect = generate_item_effect(rng);

        let mut entity: Entity = simulation.new_entity();

        entity
            .add_component(Sprite::new("assets/wrench.png".to_string()).set_layer(2))
            .unwrap();
        let mut transf = Transform::new();

        transf.scale = Vec2::new(40.0, 40.0);
        transf.position = position;
        entity.add_component(transf).unwrap();
        let kinematic = Kinematic::new();
        entity.add_component(kinematic).unwrap();

        entity
            .add_component(Item {
                effect,
                active_frames: 0,
            })
            .unwrap();

        entity
            .add_component(
                CollisionBox::new(CollisionShape::Circle { radius: 12. }, 0, 3).set_area_masks(3),
            )
            .unwrap();
        
        entity.set_sequence(10)
    }

    pub fn check_expire(&self) -> bool {
        self.active_frames > EXPIRATION_FRAMES
    }
}

#[async_trait(?Send)]
impl Component for Item {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let collision = entity.get_component::<CollisionBox>().unwrap();

        self.active_frames += 1;

        let collider_ids: Vec<EntityID> = collision.get_colliders();

        for id in collider_ids {
            let Ok(collider) = simulation.get_entity(id) else {
                continue;
            };

            if collider.get_component::<Player>().is_ok() {
                self.effect.apply(collider);
                let player_transf = collider.get_component::<Transform>().unwrap();
                let position = player_transf.position;

                simulation.enqueue_remove(entity.get_id());

                self.effect.add_powerup_effect(simulation, position);
            }
        }

        if self.check_expire() {
            simulation.enqueue_remove(entity.get_id());
        }

        Ok(())
    }
}
