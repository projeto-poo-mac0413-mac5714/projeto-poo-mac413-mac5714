use mq_game::prelude::*;

use crate::item::item_decorator::{HealingDecorator, ShooterDecorator, SpeedDecorator};

pub struct HealingEffect;
pub struct SpeedEffect;
pub struct SpecialBulletEffect;
pub struct DummyEffect;

const EFFECT_SIZE: f32 = 200.;
pub trait ItemEffect {
    fn apply(&self, player_entity: &mut Entity);

    fn add_powerup_effect(&self, simulation: &mut Simulation, position: Vec2) {
        add_powerup_effect(
            simulation,
            position,
            EFFECT_SIZE,
            11,
            1,
            "assets/effects/disintegrate_32.png",
        );
    }
}

impl ItemEffect for HealingEffect {
    fn apply(&self, player_entity: &mut Entity) {
        if let Ok(active_heal) = player_entity.get_component::<HealingDecorator>() {
            active_heal.reset_effect();
        } else {
            let mut heal_effect = HealingDecorator::new();
            heal_effect.activate_effect();
            player_entity.add_component(heal_effect).unwrap();
        }
    }
}

impl ItemEffect for SpeedEffect {
    fn apply(&self, player_entity: &mut Entity) {
        if let Ok(active_speed) = player_entity.get_component::<SpeedDecorator>() {
            active_speed.reset_effect();
        } else {
            let mut speed_effect = SpeedDecorator::new();

            speed_effect.activate_effect();
            player_entity.add_component(speed_effect).unwrap();
        }
    }
}

impl ItemEffect for SpecialBulletEffect {
    fn apply(&self, player_entity: &mut Entity) {
        if let Ok(active_shooter) = player_entity.get_component::<ShooterDecorator>() {
            active_shooter.reset_effect();
        } else {
            let mut shooter_effect = ShooterDecorator::new();
            shooter_effect.activate_effect();
            player_entity.add_component(shooter_effect).unwrap();
        }
    }
}

impl ItemEffect for DummyEffect {
    fn apply(&self, _player_entity: &mut Entity) {}
    fn add_powerup_effect(&self, simulation: &mut Simulation, position: Vec2) {
        add_powerup_effect(
            simulation,
            position,
            EFFECT_SIZE,
            6,
            5,
            "assets/effects/poof_32.png",
        );
    }
}

fn add_powerup_effect(
    simulation: &mut Simulation,
    position: Vec2,
    size: f32,
    hcells: i32,
    vcells: i32,
    path: &'static str,
) {
    let mut powerup: Entity = simulation.new_entity().set_sequence(100);

    // powerup transform
    let mut transf = Transform::new();
    transf.position = position;
    transf.scale = Vec2::new(size, size);
    powerup.add_component(transf).unwrap();
    powerup
        .add_component(AnimatedSprite::new().set_frame_rate(0.5).repeat(false))
        .unwrap();
    powerup
        .add_component(
            Sprite::new(path.to_string())
                .set_reverse_frames(false)
                .crop(hcells, vcells)
                .set_layer(0),
        )
        .unwrap();

    simulation.add_entity(powerup).unwrap();
}
