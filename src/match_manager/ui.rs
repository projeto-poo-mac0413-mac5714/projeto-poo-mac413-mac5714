use mq_game::prelude::{async_trait, Component, Entity, Label, Menu, Simulation, Vec2, WHITE};
use std::io;

use crate::match_manager::MatchManager;

pub struct UI {
    pub game_over: bool,
    pub game_loaded: bool,
}

impl UI {
    pub fn new_entity(simulation: &mut Simulation) -> Entity {
        let mut entity = simulation.new_entity().set_sequence(100);
        entity
            .add_component(Label {
                text: "".into(),
                visible: false,
                position: Vec2::new(0., 400.),
                font_size: 150.,
                color: WHITE,
            })
            .unwrap();

        entity.add_component(Menu::new()).unwrap();

        entity
            .add_component(Self {
                game_over: false,
                game_loaded: false,
            })
            .unwrap();

        entity
    }

    fn show_game_over(&mut self, entity: &mut Entity) {
        entity.get_component::<Label>().unwrap().visible = true;
    }

    pub fn register(simulation: &mut Simulation) -> Result<(), &str> {
        let ui = UI::new_entity(simulation);
        simulation.add_entity(ui)
    }
}

#[async_trait(?Send)]
impl Component for UI {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let menu = entity.get_component::<Menu>().unwrap();
        // arrumar condição de corrida
        if !self.game_loaded && menu.selected {
            MatchManager::new_match(simulation, menu.map_selected).await;

            self.game_loaded = true;
        }

        if self.game_over {
            self.show_game_over(entity);
        }
        Ok(())
    }

    fn sequence(&self) -> i32 {
        100
    }
}
