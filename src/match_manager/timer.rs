use std::io;

use mq_game::prelude::*;

pub struct Timer {
    frames: u32,
}

impl Timer {
    pub fn new(frames: u32) -> Self {
        Timer { frames }
    }

    pub fn finished(&self) -> bool {
        self.frames == 0
    }
}

#[async_trait(?Send)]
impl Component for Timer {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        if self.frames > 0 {
            self.frames -= 1;
        }

        Ok(())
    }
}
