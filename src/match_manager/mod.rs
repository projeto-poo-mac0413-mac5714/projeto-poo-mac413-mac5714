use mq_game::prelude::*;
use std::io;

use crate::{
    bullet::Bullet,
    map::{self, Tile},
    player::{self, Player},
    Args, item::Item,
};

mod timer;
mod ui;

pub use ui::*;

use self::timer::Timer;
pub struct MatchManager {
    pub config: Args,
}

impl MatchManager {
    pub fn new_entity(simulation: &mut Simulation, config: Args) -> Entity {
        let mut entity = simulation.new_entity().set_sequence(100);
        entity
            .add_component(Label {
                text: "".into(),
                visible: false,
                position: Vec2::new(0., 400.),
                font_size: 150.,
                color: WHITE,
            })
            .unwrap();

        let _ = entity.add_component(MatchManager { config });
        entity
    }

    pub async fn new_match(simulation: &mut Simulation, map: &str) {
        let mut match_manager = simulation.query::<MatchManager>();
        let match_manager = match_manager[0].get_component::<MatchManager>().unwrap();

        let config = match_manager.config.clone();

        let file_content = String::from_utf8(load_file(map).await.unwrap()).unwrap();
        let tilemap = map::parse::<map::XMLMapFactory>(&file_content);
        map::build_map::<map::XMLMapFactory>(simulation, tilemap);

        player::build_players(simulation, config)
    }

    pub fn register(simulation: &mut Simulation, config: Args) -> Result<(), &str> {
        let manager = MatchManager::new_entity(simulation, config);
        simulation.add_entity(manager)
    }
}

#[async_trait(?Send)]
impl Component for MatchManager {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let ui_ent = &mut simulation.query::<UI>()[0];
        let ui = ui_ent.get_component::<UI>().unwrap();
        let game_loaded = ui.game_loaded;
        let mut game_over = false;
        let mut winner_id = 0;

        if game_loaded {
            let mut alive_players = simulation.query::<Player>();

            if !alive_players.is_empty() {
                if alive_players.len() <= 1 {
                    let winner = &mut alive_players[0];
                    winner_id = winner.get_component::<Player>().unwrap().get_player_id();
                    game_over = true;
                }
                if game_over {
                    let ui_ent = &mut simulation.query::<UI>()[0];
                    let ui = ui_ent.get_component::<UI>().unwrap();
                    ui.game_over = true;
                    ui_ent.get_component::<Label>().unwrap().text =
                        format!("PLAYER {winner_id} WON");

                    if entity.get_component::<Timer>().is_err() {
                        entity.add_component(Timer::new(300)).unwrap();
                    }
                }
            }
        }

        if let Ok(timer) = entity.get_component::<Timer>() {
            if timer.finished() {
                entity.remove_component::<Timer>().unwrap();

                let mut ids_to_remove: Vec<EntityID> = vec![];

                // let ui_id = simulation.query::<UI>()[0].get_id();

                ids_to_remove.append(&mut entity_ids::<UI>(simulation));
                ids_to_remove.append(&mut entity_ids::<Player>(simulation));
                ids_to_remove.append(&mut entity_ids::<Bullet>(simulation));
                ids_to_remove.append(&mut entity_ids::<Tile>(simulation));
                ids_to_remove.append(&mut entity_ids::<Item>(simulation));

                for id in ids_to_remove.iter() {
                    simulation.remove_entity(*id).unwrap();
                }

                UI::register(simulation).unwrap();
            }
        }

        Ok(())
    }
}

fn entity_ids<C: Component>(simulation: &mut Simulation) -> Vec<EntityID> {
    simulation.query::<C>().iter().map(entity_to_id).collect()
}

fn entity_to_id(entity: &&mut Entity) -> EntityID {
    entity.get_id()
}
