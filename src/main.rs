use ::commands::CommandManager;
use commands::*;
use futures::executor::block_on;

use item::ItemManager;
use match_manager::{MatchManager, UI};
use mq_game::prelude::*;
use player::EventManager;

#[cfg(not(target_arch = "wasm32"))]
use clap::{arg, command, Parser};

mod bullet;
mod commands;
mod item;
mod map;
mod match_manager;
mod player;
#[cfg(test)]
mod test;

const GAME_WIDTH: i32 = 800;
const GAME_HEIGHT: i32 = 800;

#[cfg(target_arch = "wasm32")]
#[derive(Debug, Clone, Default)]
pub struct Args {
    /// Host a game
    host: bool,

    /// Connect to a game - need server IPv4
    connect: Option<String>,

    /// Save replay from file
    save_replay: Option<String>,

    /// Load replay from file
    load_replay: Option<String>,
}

#[cfg(not(target_arch = "wasm32"))]
#[derive(Debug, Clone, Parser, Default)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Host a game
    #[arg(long)]
    host: bool,

    /// Connect to a game - need server IPv4
    #[arg(short, long, value_name = "IP")]
    connect: Option<String>,

    /// Save replay from file
    #[arg(short, long, value_name = "FILE_PATH")]
    save_replay: Option<String>,

    /// Load replay from file
    #[arg(short, long, value_name = "FILE_PATH")]
    load_replay: Option<String>,
}

async fn _main() {
    #[cfg(not(target_arch = "wasm32"))]
    let config = Args::parse();
    #[cfg(target_arch = "wasm32")]
    let config = Args::default();

    let mut simulation = Simulation::new();

    UI::register(&mut simulation).unwrap();
    ItemManager::register(&mut simulation, GAME_WIDTH, GAME_HEIGHT).unwrap();
    EventManager::register(&mut simulation).unwrap();
    MatchManager::register(&mut simulation, config.clone()).unwrap();
    CommandManager::register(&mut simulation).unwrap();
    TextureManager::register(&mut simulation).unwrap();
    CollisionManager::register(&mut simulation).unwrap();

    register_commands(&mut simulation);

    let mut game = new_game_window("Tanquinhos", "", GAME_WIDTH, GAME_HEIGHT, simulation);

    game.run();
}

fn main() {
    block_on(_main());
}
