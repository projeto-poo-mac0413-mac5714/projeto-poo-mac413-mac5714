use commands::*;
use mq_game::prelude::*;

use crate::player::EventManager;

pub struct NetworkSendCommandFactory {
    factory: Box<dyn CommandFactory>,
    peer: PeerID,
}

pub struct NetworkReceiveCommandFactory {
    player_entity_id: EntityID,
    peer: PeerID,
}

impl NetworkSendCommandFactory {
    pub fn new(factory: Box<dyn CommandFactory>, peer: PeerID) -> Self {
        Self { factory, peer }
    }
}

#[async_trait(?Send)]
impl CommandFactory for NetworkSendCommandFactory {
    async fn generate_commands(&mut self, simulation: &mut Simulation) -> Vec<Command> {
        let mut commands = self.factory.generate_commands(simulation).await;

        let mut network_manager_entity = simulation.query::<NetworkManager>();
        let network_manager = network_manager_entity
            .back_mut()
            .unwrap()
            .get_component::<NetworkManager>()
            .unwrap();

        let mut packet = String::new();

        // packet = packet.into_iter().chain(self.id.to_be_bytes().into_iter()).collect();

        for command in &commands {
            // packet.push(command.get_id());
            packet.push(char::from_digit(command.get_id() as u32, 10).unwrap());
            // packet = format!("{packet}{}", command.get_id());
        }

        packet.push('\0');

        let peer = network_manager.peer(self.peer).unwrap();
        peer.push_packet(packet.as_bytes().to_owned());

        if let Some(packet) = peer.pop_packet() {
            commands = packet_to_commands(packet, simulation);
        } else {
            commands = vec![];
        }
        commands
    }
}

impl NetworkReceiveCommandFactory {
    pub fn new(peer: PeerID, player_entity_id: EntityID) -> Self {
        Self {
            peer,
            player_entity_id,
        }
    }
}

#[async_trait(?Send)]
impl CommandFactory for NetworkReceiveCommandFactory {
    async fn generate_commands(&mut self, simulation: &mut Simulation) -> Vec<Command> {
        let event_emitter = simulation.get_resource::<EventManager>().unwrap();

        let destroyed = event_emitter.is_event_emitted("destroyed".into(), self.player_entity_id);

        let mut network_manager_entity = simulation.query::<NetworkManager>();
        let network_manager = network_manager_entity
            .back_mut()
            .unwrap()
            .get_component::<NetworkManager>()
            .unwrap();

        let peer = network_manager.peer(self.peer).unwrap();

        if destroyed {
            peer.close();
            return vec![];
        }

        let mut commands = Vec::<Command>::new();

        if let Some(packet) = peer.pop_packet() {
            commands = packet_to_commands(packet, simulation);
        }
        commands
    }
}

fn packet_to_commands(packet: Packet, simulation: &mut Simulation) -> Vec<Command> {
    let mut comms = vec![];
    let packet2 = String::from_utf8(packet.payload).unwrap();

    for id in packet2.chars() {
        let id = if let Ok(id) = String::from(id).parse() {
            id
        } else {
            continue;
        };

        if let Some(command) = simulation
            .get_resource::<CommandManager>()
            .unwrap()
            .command_from_id(id)
        {
            comms.push(command)
        }
    }

    // let s = String::from_utf8(packet.payload).unwrap();

    // for id in packet.payload {
    //     if let Some(command) = command_from_id(id) {
    //         comms.push(command);
    //     }
    // }

    comms
}
