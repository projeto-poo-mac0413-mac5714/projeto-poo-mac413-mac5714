use commands::CommandFactory;
use mq_game::prelude::*;

mod input_factory;
mod networked_factories;
mod replay_factories;

pub use input_factory::*;
pub use networked_factories::*;
pub use replay_factories::*;

use super::Command;

pub struct DummyCommandFactory;

#[async_trait(?Send)]
impl CommandFactory for DummyCommandFactory {
    async fn generate_commands(&mut self, _simulation: &mut Simulation) -> Vec<Command> {
        Vec::new()
    }
}
