use commands::*;
use mq_game::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub struct Key {
    pub key: KeyCode,
    pub to_hold: bool,
}

pub struct LocalInputCommandFactory {
    mapping: Vec<(Key, Command)>,
}

impl LocalInputCommandFactory {
    pub fn new(mapping: Vec<(Key, Command)>) -> Result<Self, ()> {
        Ok(Self { mapping })
    }
}

#[async_trait(?Send)]
impl CommandFactory for LocalInputCommandFactory {
    async fn generate_commands(&mut self, _simulation: &mut Simulation) -> Vec<Command> {
        let mut comms = Vec::<Command>::new();
        for tuple in &self.mapping {
            let button = &tuple.0;
            if (Input::is_key_down(button.key) && button.to_hold)
                || (Input::is_key_just_pressed(button.key) && !button.to_hold)
            {
                comms.push(tuple.1.clone());
            }
        }
        comms
    }
}
