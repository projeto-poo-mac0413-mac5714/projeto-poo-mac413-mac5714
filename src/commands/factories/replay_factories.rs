use std::{
    fs::File,
    io::{BufRead, BufReader, Write},
};

use commands::*;
use mq_game::prelude::*;

use crate::player::PlayerID;

pub struct WriteReplayCommandFactory {
    pub player_id: PlayerID,
    pub factory: Box<dyn CommandFactory>,
    pub file: File,
}

pub struct ReadReplayCommandFactory {
    pub player_id: PlayerID,
    pub file: BufReader<File>,
}

#[async_trait(?Send)]
impl CommandFactory for WriteReplayCommandFactory {
    async fn generate_commands(&mut self, simulation: &mut Simulation) -> Vec<Command> {
        let commands = self.factory.generate_commands(simulation).await;

        let mut packet = String::new();

        packet.push(char::from_digit(self.player_id as u32, 10).unwrap());

        for command in &commands {
            packet.push(char::from_digit(command.get_id() as u32, 10).unwrap());
        }

        packet.push('0');
        packet.push('\n');

        self.file.write(packet.as_bytes()).unwrap();

        self.file.sync_all().unwrap();

        self.file.flush().unwrap();

        commands
    }
}

#[async_trait(?Send)]
impl CommandFactory for ReadReplayCommandFactory {
    async fn generate_commands(&mut self, simulation: &mut Simulation) -> Vec<Command> {
        let mut commands = Vec::<Command>::new();
        loop {
            let mut line = String::new();

            if self.file.read_line(&mut line).is_err() {
                return commands;
            }

            if line.is_empty() {
                return commands;
            }

            let (player_id, ids) = line.split_at(1);

            if String::from(player_id).parse::<PlayerID>().unwrap() == self.player_id {
                let ids = String::from(ids)
                    .chars()
                    .filter_map(|c| String::from(c).parse::<CommandID>().ok())
                    .collect::<Vec<_>>();

                for id in ids {
                    if let Some(command) = simulation
                        .get_resource::<CommandManager>()
                        .unwrap()
                        .command_from_id(id)
                    {
                        commands.push(command)
                    }
                }

                return commands;
            }
        }
    }
}
