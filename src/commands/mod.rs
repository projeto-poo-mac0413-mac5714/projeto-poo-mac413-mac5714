use crate::bullet::Shooter;
use commands::*;
use core::f32::consts::PI;
use mq_game::prelude::*;

mod factories;

pub use factories::*;

pub struct MoveForwardCommand;
pub struct MoveBackwardCommand;
pub struct RotateLeftCommand;
pub struct RotateRightCommand;
pub struct ShootCommand;

pub fn register_commands(simulation: &mut Simulation) {
    let manager = simulation.get_resource::<CommandManager>().unwrap();

    manager.register_command(MoveForwardCommand).unwrap();
    manager.register_command(MoveBackwardCommand).unwrap();
    manager.register_command(RotateLeftCommand).unwrap();
    manager.register_command(RotateRightCommand).unwrap();
    manager.register_command(ShootCommand).unwrap();
}

impl CommandTrait for MoveForwardCommand {
    fn execute(&self, entity: &mut Entity, _simulation: &mut Simulation) {
        let kin = entity.get_component::<Kinematic>().unwrap();

        kin.velocity.y += 3000.0;
    }

    fn clone(&self) -> Box<dyn CommandTrait> {
        Box::new(MoveForwardCommand)
    }
}

impl CommandTrait for MoveBackwardCommand {
    fn execute(&self, entity: &mut Entity, _simulation: &mut Simulation) {
        let kin = entity.get_component::<Kinematic>().unwrap();

        kin.velocity.y -= 3000.0;
    }
    fn clone(&self) -> Box<dyn CommandTrait> {
        Box::new(MoveBackwardCommand)
    }
}

impl CommandTrait for RotateLeftCommand {
    fn execute(&self, entity: &mut Entity, _simulation: &mut Simulation) {
        let kin = entity.get_component::<Kinematic>().unwrap();

        kin.angular_velocity -= PI * 10.0;
    }

    fn clone(&self) -> Box<dyn CommandTrait> {
        Box::new(RotateLeftCommand)
    }
}

impl CommandTrait for RotateRightCommand {
    fn execute(&self, entity: &mut Entity, _simulation: &mut Simulation) {
        let kin = entity.get_component::<Kinematic>().unwrap();

        kin.angular_velocity += PI * 10.0;
    }

    fn clone(&self) -> Box<dyn CommandTrait> {
        Box::new(RotateRightCommand)
    }
}

impl CommandTrait for ShootCommand {
    fn execute(&self, entity: &mut Entity, simulation: &mut Simulation) {
        let transform = *entity.get_component::<Transform>().unwrap();
        entity
            .get_component::<Shooter>()
            .unwrap()
            .shoot(simulation, transform);
    }
    fn clone(&self) -> Box<dyn CommandTrait> {
        Box::new(ShootCommand)
    }
}
