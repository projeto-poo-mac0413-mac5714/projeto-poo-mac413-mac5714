use mq_game::prelude::*;
use std::io;

use super::TileStrategy;

pub struct Tile {
    strategy: Option<Box<dyn TileStrategy>>,
}

impl Tile {
    pub fn new(
        simulation: &mut Simulation,
        x: f32,
        y: f32,
        sprite: Option<SpriteParameter>,
        has_collision: bool,
        strategy: Option<Box<dyn TileStrategy>>,
    ) -> Entity {
        let mut entity: Entity = simulation.new_entity();

        if let Some(sprite) = sprite {
            let tile_sprite = match sprite {
                SpriteParameter::PathToSprite(s) => Sprite::new(s),
                SpriteParameter::LoadedSprite(s) => s,
            };
            entity.add_component(tile_sprite.set_layer(-2)).unwrap();
        }

        let mut transf = Transform::new();
        transf.position = Vec2::new(x, y);
        transf.scale = Vec2::new(50.0, 50.0);

        entity.add_component(transf).unwrap();
        entity.add_component(Tile { strategy }).unwrap();

        if has_collision {
            entity
                .add_component(
                    CollisionBox::new(
                        CollisionShape::Rectangle {
                            width: 50.,
                            height: 50.,
                        },
                        1,
                        0,
                    )
                    .set_area_masks(2),
                )
                .unwrap();
        }

        entity
    }
}

#[async_trait(?Send)]
impl Component for Tile {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let collision = entity.get_component::<CollisionBox>();

        // Apply tile strategy on collision
        if let Ok(collision) = collision {
            if !collision.get_colliders().is_empty() {
                if let Some(strategy) = self.strategy.as_mut() {
                    strategy.on_collision(entity, simulation);
                }
            }
        }
        Ok(())
    }
}
