mod factories;
mod tile;
mod tile_strategy;

pub use factories::*;
use regex::Regex;
use std::collections::HashMap;
pub use tile::*;
pub use tile_strategy::*;

pub struct Layer {
    pub layer_id: i32,
    pub layer_name: String,
    pub data: Vec<Vec<i32>>,
}

pub struct TileSet {
    pub filename: String,
    pub firstgid: i32,
    pub rows: i32,
    pub columns: i32,
}

pub struct TileMap {
    pub layers: Vec<Layer>,
    pub properties: HashMap<i32, HashMap<String, String>>,
    pub tilesets: HashMap<String, TileSet>,
}

impl Layer {
    pub fn parse_data(text_data: String, width: i32, height: i32) -> Vec<Vec<i32>> {
        let re = Regex::new(r"^\s+").unwrap();
        let text = re.replace_all(&text_data, "").to_string();
        let mut array = vec![vec![0; width as usize]; height as usize];

        for (y, line) in text.lines().enumerate() {
            for (x, value) in line
                .split_terminator(',')
                .map(|s| s.parse::<i32>().unwrap())
                .enumerate()
            {
                array[x][y] = value;
            }
        }

        array
    }
}

impl TileMap {
    pub fn new() -> TileMap {
        TileMap {
            layers: Vec::new(),
            properties: HashMap::new(),
            tilesets: HashMap::new(),
        }
    }

    pub fn push(&mut self, element: Layer) {
        self.layers.push(element);
    }
}
