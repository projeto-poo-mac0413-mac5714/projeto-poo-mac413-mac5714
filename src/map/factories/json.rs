use std::fs::read_to_string;

use serde::{Deserialize, Serialize};
use serde_json::{Result};

use crate::map::{Layer, TileMap};

use super::MapFactory;

#[derive(Serialize, Deserialize)]
pub struct Tile {
    name: String,
    _type: String,
    value: bool
}

pub struct JSONMapFactory;

impl MapFactory for JSONMapFactory {
    fn parse(filename: &str) -> TileMap {
        let file_str = read_to_string(filename).unwrap();
        let mut reader = Reader::from_str(&file_str);
        reader.trim_text(true);

        let mut buf = Vec::new();
        let mut tilemap: TileMap = TileMap::new();

        loop {
            match reader.read_event_into(&mut buf) {
                Err(e) => panic!("Error {e}"),

                Ok(Event::Eof) => break,

                Ok(Event::Start(e)) => match e.name().as_ref() {
                    b"layer" => {
                        let id = get_i32_from_xml(&e, "id");

                        let name = get_str_from_xml(&e, "name");

                        let width = get_i32_from_xml(&e, "width");

                        let height = get_i32_from_xml(&e, "height");

                        // discard start of data event to make code less verbose
                        let _ = reader.read_event();

                        // read from start of data tag to the end
                        let start = BytesStart::new("data");
                        let end = start.to_end().into_owned();

                        let text = reader.read_text(end.name()).unwrap().to_string();

                        let layer = Layer {
                            layer_id: id,
                            layer_name: name,
                            data: Layer::parse_data(text, width, height),
                        };

                        tilemap.push(layer);
                    }
                    _ => {}
                },

                _ => (),
            }
        }

        tilemap
    }
}

fn get_str_from_xml(e: &BytesStart<'_>, attr_name: &str) -> String {
    let attr = e
        .try_get_attribute(attr_name)
        .unwrap()
        .unwrap()
        .unescape_value()
        .unwrap()
        .to_string();
    attr
}

fn get_i32_from_xml(e: &BytesStart<'_>, attr_name: &str) -> i32 {
    let attr = e
        .try_get_attribute(attr_name)
        .unwrap()
        .unwrap()
        .unescape_value()
        .unwrap()
        .parse::<i32>()
        .unwrap();
    attr
}
