use std::collections::HashMap;



use quick_xml::{
    events::{BytesStart, Event},
    Reader,
};
// use reqwest::get;

use crate::map::{Layer, TileMap, TileSet};

use super::MapFactory;
pub struct XMLMapFactory;

impl MapFactory for XMLMapFactory {
    fn parse(file_content: &str) -> TileMap {
        let mut reader = Reader::from_str(file_content);
        reader.trim_text(true);

        let mut buf = Vec::new();
        let mut tilemap: TileMap = TileMap::new();

        let mut firstgid: i32 = 1;
        let mut tileset_name: String = "".to_string();
        loop {
            match reader.read_event_into(&mut buf) {
                Err(e) => panic!("Error {e}"),

                Ok(Event::Eof) => break,

                Ok(Event::Start(e)) => match e.name().as_ref() {
                    b"tileset" => {
                        firstgid = get_i32_from_xml(&e, "firstgid");
                        tileset_name = get_str_from_xml(&e, "name");
                        let columns = get_i32_from_xml(&e, "columns");
                        let tilecount = get_i32_from_xml(&e, "tilecount");
                        let rows = tilecount / columns;

                        let filename = match reader.read_event() {
                            Ok(Event::Empty(inner_e)) => match inner_e.name().as_ref() {
                                b"image" => {
                                    let mut file_path = String::from("assets/");
                                    let path_from_xml = get_str_from_xml(&inner_e, "source");
                                    file_path.push_str(path_from_xml.as_str());
                                    file_path
                                }

                                _ => "".to_string(),
                            },
                            _ => "".to_string(),
                        };
                        tilemap.tilesets.insert(
                            tileset_name.clone(),
                            TileSet {
                                filename,
                                firstgid,
                                rows,
                                columns,
                            },
                        );
                    }

                    b"layer" => {
                        let id = get_i32_from_xml(&e, "id");

                        let name = get_str_from_xml(&e, "name");

                        let width = get_i32_from_xml(&e, "width");

                        let height = get_i32_from_xml(&e, "height");

                        // discard start of data event to make code less verbose
                        let _ = reader.read_event();

                        // read from start of data tag to the end
                        let start = BytesStart::new("data");
                        let end = start.to_end().into_owned();

                        let text = reader.read_text(end.name()).unwrap().to_string();

                        let layer = Layer {
                            layer_id: id,
                            layer_name: name,
                            data: Layer::parse_data(text, width, height),
                        };

                        tilemap.push(layer);
                    }

                    b"tile" => {
                        let tile_id: i32 = get_i32_from_xml(&e, "id") + firstgid;
                        let mut properties_hashmap = HashMap::new();
                        properties_hashmap.insert("tileset".to_string(), tileset_name.clone());
                        loop {
                            match reader.read_event() {
                                Ok(Event::Empty(inner_e)) => match inner_e.name().as_ref() {
                                    b"property" => {
                                        let name = get_str_from_xml(&inner_e, "name");
                                        let value = get_str_from_xml(&inner_e, "value");
                                        properties_hashmap.insert(name, value);
                                    }

                                    _ => {}
                                },

                                Ok(Event::End(_inner_e)) => break,

                                _ => (),
                            }
                        }
                        tilemap.properties.insert(tile_id, properties_hashmap);
                    }
                    _ => (),
                },

                _ => (),
            }
        }

        tilemap
    }
}

fn get_str_from_xml(e: &BytesStart<'_>, attr_name: &str) -> String {
    let attr = e
        .try_get_attribute(attr_name)
        .unwrap()
        .unwrap()
        .unescape_value()
        .unwrap()
        .to_string();
    attr
}

fn get_i32_from_xml(e: &BytesStart<'_>, attr_name: &str) -> i32 {
    let attr = e
        .try_get_attribute(attr_name)
        .unwrap()
        .unwrap()
        .unescape_value()
        .unwrap()
        .parse::<i32>()
        .unwrap();
    attr
}
