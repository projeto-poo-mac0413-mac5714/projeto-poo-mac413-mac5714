mod xml;

use mq_game::prelude::{Simulation, Sprite, SpriteParameter, Vec2};
use std::collections::HashMap;
pub use xml::*;

use crate::commands::DummyCommandFactory;
use crate::player::Player;

use super::{DestroyStrategy, ReflectStrategy, Tile, TileMap, TileSet, TileStrategy};

// Blocks' size
const SCALE: f32 = 50.0;
const NONE: i32 = 0;

// Game Objects IDs
const TANK: i32 = 1;
const FLOOR: i32 = 2;
const WALL: i32 = 3;
const HOLE: i32 = 4;

const REFLECT: i32 = 10;
const DESTROY: i32 = 20;

pub struct MappedTile {
    path: String,
    strategy_id: i32,
    obj_id: i32,
    hcells: i32,
    vcells: i32,
    cell: i32,
}

impl MappedTile {
    #[allow(unused)]
    pub fn new() -> MappedTile {
        MappedTile {
            path: String::from(""),
            strategy_id: NONE,
            obj_id: NONE,
            hcells: 0,
            vcells: 0,
            cell: 0,
        }
    }
}

pub trait MapFactory {
    fn parse(file_content: &str) -> TileMap;
}

pub fn parse<T: MapFactory>(file_content: &str) -> TileMap {
    T::parse(file_content)
}

// pub fn build_map<T: MapFactory>(simulation: &mut Simulation, filename: &str) {
pub fn build_map<T: MapFactory>(simulation: &mut Simulation, tilemap: TileMap) {
    // let tilemap = T::parse(filename);

    let player_pos = Vec2::new(1., 1.) * SCALE / 2.;

    for layer in tilemap.layers {
        for (j, line) in layer.data.iter().enumerate() {
            for (i, content) in line.iter().enumerate() {
                let _tile = *content;
                let y = (i as f32) * SCALE;
                let x = (j as f32) * SCALE;

                let props = tilemap.properties.get(content);
                match props {
                    Some(tile_properties) => {
                        let tileset_name = (*tile_properties).get("tileset").unwrap();
                        let tileset = tilemap.tilesets.get(tileset_name).unwrap();
                        let mapped_tile = map_tile_from_properties(_tile, tile_properties, tileset);

                        match mapped_tile.obj_id {
                            TANK => {
                                let id = props.unwrap().get("player_id").unwrap().parse().unwrap();
                                build_tank(mapped_tile, id, player_pos, x, y, simulation);
                            }
                            WALL => {
                                build_wall(mapped_tile, simulation, x, y);
                            }
                            FLOOR => {
                                build_floor(mapped_tile, simulation, x, y);
                            }

                            _ => {}
                        }
                    }

                    _ => {}
                };
            }
        }
    }
}

pub fn build_floor(mapped_tile: MappedTile, simulation: &mut Simulation, x: f32, y: f32) {
    let floor_sprite = SpriteParameter::LoadedSprite(
        Sprite::new(mapped_tile.path)
            // .set_texture(mapped_tile.texture)
            .crop(mapped_tile.hcells, mapped_tile.vcells)
            .set_frame(mapped_tile.cell),
    );
    let floor = Tile::new(
        simulation,
        x + SCALE / 2.,
        y + SCALE / 2.,
        Some(floor_sprite),
        false,
        None,
    );

    simulation.add_entity(floor).unwrap();
}

pub fn build_wall(mapped_tile: MappedTile, simulation: &mut Simulation, x: f32, y: f32) {
    // Define Reflect strategy for walls on the border and destroy strategy for the others walls
    let strategy: Option<Box<dyn TileStrategy>> = if mapped_tile.strategy_id == REFLECT {
        Some(Box::new(ReflectStrategy))
    } else {
        Some(Box::new(DestroyStrategy {
            collision_count: 1,
            strategy: Box::new(ReflectStrategy),
        }))
    };

    let wall_sprite = SpriteParameter::LoadedSprite(
        Sprite::new(mapped_tile.path)
            // .set_texture(mapped_tile.texture)
            .crop(mapped_tile.hcells, mapped_tile.vcells)
            .set_frame(mapped_tile.cell),
    );

    let wall = Tile::new(
        simulation,
        x + SCALE / 2.,
        y + SCALE / 2.,
        Some(wall_sprite),
        true,
        strategy,
    );

    simulation.add_entity(wall).unwrap();
}

pub fn build_tank(
    mapped_tile: MappedTile,
    id: u8,
    player_pos: Vec2,
    x: f32,
    y: f32,
    _simulation: &mut Simulation,
) {
    let pos = player_pos + Vec2::new(x, y);

    let player_sprite = SpriteParameter::LoadedSprite(
        Sprite::new(mapped_tile.path)
            // .set_texture(mapped_tile.texture)
            .crop(mapped_tile.hcells, mapped_tile.vcells)
            .set_frame(mapped_tile.cell),
    );

    let player = Player::new(_simulation, pos, DummyCommandFactory, player_sprite, id);
    _simulation.add_entity(player).unwrap();
}

pub fn map_tile_from_properties(
    tile_position: i32,
    properties: &HashMap<String, String>,
    tileset: &TileSet,
) -> MappedTile {
    let mut tile: MappedTile = MappedTile {
        path: tileset.filename.to_owned(),
        vcells: tileset.rows,
        hcells: tileset.columns,
        // cell: tile_position - tileset.firstgid,
        cell: tile_position - tileset.firstgid,
        strategy_id: NONE,
        obj_id: NONE,
    };
    match properties.get("type") {
        Some(tile_type) => match (*tile_type).as_ref() {
            "floor" => {
                tile.obj_id = FLOOR;
            }

            "wall" => {
                tile.obj_id = WALL;
            }

            "hole" => {
                tile.obj_id = HOLE;
            }

            "player" => {
                tile.obj_id = TANK;
            }

            _ => {}
        },
        _ => {}
    };

    match properties.get("strategy") {
        Some(tile_type) => match (*tile_type).as_ref() {
            "reflect" => {
                tile.strategy_id = REFLECT;
            }

            "destroy" => {
                tile.strategy_id = DESTROY;
            }

            _ => {}
        },
        _ => {}
    };

    tile
}
