mod destroy_strategy;
mod reflect_strategy;

pub use destroy_strategy::*;
use mq_game::prelude::{Entity, Simulation};
pub use reflect_strategy::*;
pub trait TileStrategy {
    fn on_collision(&mut self, entity: &mut Entity, simulation: &mut Simulation);
}
