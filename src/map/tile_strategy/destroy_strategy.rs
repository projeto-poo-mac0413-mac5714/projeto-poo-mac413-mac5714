use mq_game::prelude::{Entity, Simulation};

use super::TileStrategy;

pub struct DestroyStrategy {
    pub collision_count: i32,
    pub strategy: Box<dyn TileStrategy>,
}

impl TileStrategy for DestroyStrategy {
    fn on_collision(&mut self, entity: &mut Entity, simulation: &mut Simulation) {
        self.collision_count -= 1;

        if self.collision_count <= 0 {
            simulation.enqueue_remove(entity.get_id());
        }
        self.strategy.on_collision(entity, simulation);
    }
}
