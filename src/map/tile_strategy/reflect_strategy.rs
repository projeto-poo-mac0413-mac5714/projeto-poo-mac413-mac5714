use mq_game::prelude::{CollisionBox, Entity, Kinematic, Simulation, Transform, Vec2, DELTA};

use super::TileStrategy;

pub struct ReflectStrategy;

impl TileStrategy for ReflectStrategy {
    fn on_collision(&mut self, entity: &mut Entity, simulation: &mut Simulation) {
        let transform = *entity.get_component::<Transform>().unwrap();
        let collision = entity.get_component::<CollisionBox>().unwrap();

        for collider_id in collision.get_colliders() {
            if let Ok(collider) = simulation.get_entity(collider_id) {
                let collider_relative =
                    collider.get_component::<Transform>().unwrap().position - transform.position;

                if let Some(collider_relative) = collider_relative.try_normalize() {
                    let kinematic = collider.get_component::<Kinematic>().unwrap();
                    let speed = kinematic.velocity.length();

                    let reflected_velocity = reflect(collider_relative, kinematic.velocity);

                    kinematic.velocity = reflected_velocity;

                    let transform = collider.get_component::<Transform>().unwrap();
                    transform.position += 1. * DELTA * speed * collider_relative.normalize();
                }
            }
        }
    }
}

fn reflect(collider_relative: Vec2, velocity: Vec2) -> Vec2 {
    let collider_relative = collider_relative.normalize();
    let _angle = collider_relative.angle_between(Vec2::new(-1., 1.));

    let normals = [
        Vec2::new(1., 0.),
        Vec2::new(0., 1.),
        Vec2::new(-1., 0.),
        Vec2::new(0., -1.),
    ];

    let mut max = None;
    let mut normal = None;

    for n in normals {
        if max.is_none() || n.dot(collider_relative) > max.unwrap() {
            max = Some(n.dot(collider_relative));
            normal = Some(n);
        }
    }
    let normal = normal.unwrap();

    velocity - 2. * normal * velocity.dot(normal)
}
