use std::fs;

use crate::map::{
    build_floor, build_tank, build_wall, MapFactory, MappedTile, TileMap, XMLMapFactory,
};
use mq_game::prelude::{Simulation, Vec2};

#[test]
fn test_xml_parsing_no_data() {
    let xml_file: &str = "maps/test.tmx";

    let xml = fs::read_to_string(xml_file).unwrap();
    let tm: TileMap = XMLMapFactory::parse(&xml);

    let name = tm.layers[0].layer_name.clone();
    let id = tm.layers[0].layer_id;

    assert_eq!(name, "TEST TILE MAP".to_string());
    assert_eq!(id, 99);
}
#[test]
fn test_xml_parsing_with_data() {
    let xml_file: &str = "maps/test_with_data.tmx";

    let xml = fs::read_to_string(xml_file).unwrap();
    let tm: TileMap = XMLMapFactory::parse(&xml);

    let name = tm.layers[0].layer_name.clone();
    let id = tm.layers[0].layer_id;

    assert_eq!(name, "TEST TILE MAP".to_string());
    assert_eq!(id, 99);

    for i in 0..5 {
        for j in 0..5 {
            assert_eq!(22, tm.layers[0].data[i][j]);
        }
    }
}

#[test]
fn test_map_building_functions() {
    let player_pos = Vec2::new(5., 5.);
    let mut sim: Simulation = Simulation::new();

    build_tank(MappedTile::new(), 1, player_pos, 0., 0., &mut sim);
    build_wall(MappedTile::new(), &mut sim, 0., 0.);
    build_floor(MappedTile::new(), &mut sim, 0., 0.);
}
