use std::f32::consts::PI;

use futures::executor::block_on;

use mq_game::prelude::*;

use crate::map::{ReflectStrategy, Tile};

#[test]
fn updates_position_correctly() {
    let mut sim = Simulation::new();
    let mut entity = sim.new_entity();
    let id = entity.get_id();

    let transf = Transform::new();
    entity.add_component(transf).unwrap();

    let mut kin = Kinematic::new();
    kin.velocity = Vec2::new(10.0, 10.0);
    entity.add_component(kin).unwrap();

    let _ = sim.add_entity(entity);

    let mut sim: Simulation = block_on(async {
        for _i in 0..10 {
            sim = sim.update().await;
        }
        sim
    });

    let entity = sim.get_entity(id).unwrap();
    let pos = entity.get_component::<Transform>().unwrap().position;
    assert_eq!(pos.x, 100.0 * DELTA);
    assert_eq!(pos.y, 100.0 * DELTA);
}

#[test]
fn updates_velocity_correctly() {
    let mut sim = Simulation::new();
    let mut entity = sim.new_entity();
    let id = entity.get_id();

    let transf = Transform::new();
    entity.add_component(transf).unwrap();

    let mut kin = Kinematic::new();
    kin.acceleration = Vec2::new(10.0, 10.0);
    entity.add_component(kin).unwrap();

    let _ = sim.add_entity(entity);

    let mut sim: Simulation = block_on(async {
        for _i in 0..10 {
            sim = sim.update().await;
        }
        sim
    });

    let entity = sim.get_entity(id).unwrap();
    let vel = entity.get_component::<Kinematic>().unwrap().velocity;
    assert_eq!(vel.x, 100.0);
    assert_eq!(vel.y, 100.0);
}

#[test]
fn updates_rotation_correctly() {
    let mut sim = Simulation::new();
    let mut entity = sim.new_entity();
    let id = entity.get_id();

    let transf = Transform::new();
    entity.add_component(transf).unwrap();

    let mut kin = Kinematic::new();
    kin.angular_velocity = 10.0;
    entity.add_component(kin).unwrap();

    let _ = sim.add_entity(entity);

    let mut sim: Simulation = block_on(async {
        for _i in 0..10 {
            sim = sim.update().await;
        }
        sim
    });

    let entity = sim.get_entity(id).unwrap();
    let rot = entity.get_component::<Transform>().unwrap().rotation;
    assert_eq!(rot, 100.0 * DELTA);
}
#[test]
fn updates_angular_velocity_correctly() {
    let mut sim = Simulation::new();
    let mut entity = sim.new_entity();
    let id = entity.get_id();

    let transf = Transform::new();
    entity.add_component(transf).unwrap();

    let mut kin = Kinematic::new();
    kin.angular_acceleration = 10.;
    entity.add_component(kin).unwrap();

    let _ = sim.add_entity(entity);

    let mut sim: Simulation = block_on(async {
        for _i in 0..10 {
            sim = sim.update().await;
        }
        sim
    });

    let entity = sim.get_entity(id).unwrap();
    let ang_vel = entity
        .get_component::<Kinematic>()
        .unwrap()
        .angular_velocity;
    assert_eq!(ang_vel, 100.);
}
#[test]
fn updates_position_after_rotation_correctly() {
    let mut sim = Simulation::new();
    let mut entity = sim.new_entity();
    let id = entity.get_id();

    let mut transf = Transform::new();
    transf.position = Vec2::new(10., 10.);
    entity.add_component(transf).unwrap();

    let mut kin = Kinematic::new();
    kin.angular_velocity = 1000.;
    entity.add_component(kin).unwrap();

    let _ = sim.add_entity(entity);

    let mut sim: Simulation = block_on(async {
        sim = sim.update().await;
        sim
    });

    let entity = sim.get_entity(id).unwrap();
    let pos = entity.get_component::<Transform>().unwrap().position;
    let _one: f32 = 1.0;
    assert_eq!(pos.x, 10.);
    assert_eq!(pos.y, 10.);
}

#[test]
fn static_collision_works() {
    let mut sim: Simulation = Simulation::new();

    let entity_1: Entity = create_collision_test_entity(&mut sim, 50.0, 0.0, 0.0);
    let entity_2: Entity = create_collision_test_entity(&mut sim, 50.0, 0.0, 0.0);
    let entity_3: Entity = create_collision_test_entity(&mut sim, 50.0, 25.0, 25.0);
    let entity_4: Entity = create_collision_test_entity(&mut sim, 50.0, 100.0, 100.0);

    let mut collision_manager = sim.new_entity();

    collision_manager
        .add_component(CollisionManager {})
        .unwrap();

    sim.add_entity(collision_manager).unwrap();
    let id_1 = entity_1.get_id();
    let id_2 = entity_2.get_id();
    let id_3 = entity_3.get_id();
    let id_4 = entity_4.get_id();

    sim.add_entity(entity_1).unwrap();
    sim.add_entity(entity_2).unwrap();
    sim.add_entity(entity_3).unwrap();
    sim.add_entity(entity_4).unwrap();

    let mut sim: Simulation = block_on(sim.update());

    let ent_1 = sim.get_entity(id_1).unwrap();

    let box_1 = ent_1.get_component::<CollisionBox>().unwrap();

    let b1_colliders = box_1.get_colliders();

    println!("{:?}", b1_colliders);
    assert!(b1_colliders.contains(&id_2));
    assert!(b1_colliders.contains(&id_3));
    assert!(!b1_colliders.contains(&id_4));

    let ent_2 = sim.get_entity(id_2).unwrap();
    let box_2 = ent_2.get_component::<CollisionBox>().unwrap();

    let b2_colliders = box_2.get_colliders();

    assert!(b2_colliders.contains(&id_1));
    assert!(b2_colliders.contains(&id_3));
    assert!(!b2_colliders.contains(&id_4));

    let ent_3 = sim.get_entity(id_3).unwrap();
    let box_3 = ent_3.get_component::<CollisionBox>().unwrap();

    let b3_colliders = box_3.get_colliders();

    assert!(b3_colliders.contains(&id_1));
    assert!(b3_colliders.contains(&id_2));
    assert!(!b3_colliders.contains(&id_4));

    let ent_4 = sim.get_entity(id_4).unwrap();
    let box_4 = ent_4.get_component::<CollisionBox>().unwrap();

    let b4_colliders = box_4.get_colliders();

    assert!(b4_colliders.is_empty());
}

#[test]
fn reflect_correctly() {
    let mut sim: Simulation = Simulation::new();

    sim = check_reflection(sim, 0.0);
    sim = check_reflection(sim, 0.5);
    sim = check_reflection(sim, 1.0);
    sim = check_reflection(sim, -0.5);
    _ = check_reflection(sim, -1.0);
}

fn check_reflection(mut sim: Simulation, angle: f32) -> Simulation {
    let radius = 50.;
    let velocity_1 = Vec2::new(0.0, -500.0);

    let mut entity_1: Entity =
        create_collision_test_entity(&mut sim, 1.0, radius * angle.sin(), radius * angle.cos());
    let mut kinematic = Kinematic::new().set_collision_mode(CollisionMode::Reflect);

    kinematic.velocity = velocity_1.rotate(Vec2::from_angle(-angle));
    entity_1.add_component(kinematic).unwrap();

    entity_1.get_component::<CollisionBox>().unwrap().layers = 2;

    let entity_2: Entity = Tile::new(
        &mut sim,
        0.0,
        0.0,
        None,
        true,
        Some(Box::new(ReflectStrategy)),
    );

    let mut collision_manager = sim.new_entity();

    collision_manager
        .add_component(CollisionManager {})
        .unwrap();

    sim.add_entity(collision_manager).unwrap();
    let id_1 = entity_1.get_id();
    let id_2 = entity_2.get_id();

    sim.add_entity(entity_1).unwrap();
    sim.add_entity(entity_2).unwrap();

    let mut sim: Simulation = block_on(sim.update());

    for _ in 0..50 {
        sim = block_on(sim.update());

        let ent_1 = sim.get_entity(id_1).unwrap();
        let _pos_1 = ent_1.get_component::<Transform>().unwrap().position;
        let _kin_1 = ent_1.get_component::<Kinematic>().unwrap().velocity;

        let ent_2 = sim.get_entity(id_2).unwrap();
        let _pos_2 = ent_2.get_component::<Transform>().unwrap().position;

        // println!("{pos_1} {pos_2} {kin_1}");
    }

    let ent_1 = sim.get_entity(id_1).unwrap();
    let kin_1 = ent_1.get_component::<Kinematic>().unwrap();

    let expected_velocity = velocity_1.rotate(Vec2::from_angle(2. * PI - angle));

    println!("{} ~= {}", kin_1.velocity, expected_velocity);
    assert!(kin_1.velocity.abs_diff_eq(expected_velocity, 0.1));

    sim.remove_entity(id_1).unwrap();
    sim.remove_entity(id_2).unwrap();

    sim
}

fn create_collision_test_entity(
    simulation: &mut Simulation,
    radius: f32,
    x: f32,
    y: f32,
) -> Entity {
    let mut entity: Entity = simulation.new_entity();

    let collision_box = CollisionBox::new(CollisionShape::Circle { radius }, 1, 0);
    let area_box = collision_box.set_area_masks(1);
    entity.add_component(area_box).unwrap();

    let mut transform = Transform::new();
    transform.position.x = x;
    transform.position.y = y;

    entity.add_component(transform).unwrap();

    entity
}
// fn create_square_collision_test_entity(
//     simulation: &mut Simulation,
//     width: f32,
//     height: f32,
//     position: Vec2,
// ) -> Entity {
//     let mut entity: Entity = simulation.new_entity();

//     let collision_box = CollisionBox::new(CollisionShape::Rectangle { width, height }, 1, 1);
//     let area_box = collision_box.set_area_masks(1);
//     entity.add_component(area_box).unwrap();

//     let mut transform = Transform::new();
//     transform.position = position;

//     entity.add_component(transform).unwrap();

//     entity
// }
