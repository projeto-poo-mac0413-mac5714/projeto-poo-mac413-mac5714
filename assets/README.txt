
https://opengameart.org/content/tanks (made by zironid_n, CC0).
https://opengameart.org/content/32x32-dungeon-tileset (made by stealthix, CC0)
https://opengameart.org/content/explosion (made by Cuzco, CC0)
https://opengameart.org/content/electrical-disintegration-animation (made by superjoe, CC0)
https://opengameart.org/content/wrench-0 (made by Santonich, CC BY 4.0)
https://opengameart.org/content/spaceship-bullet (made by vergil1018, CC0)
https://opengameart.org/content/aztec-tileset (made by Sevarihk, CC BY 4.0)
https://opengameart.org/content/poof-effect-spritesheet (made by jellyfizh, CC0)
https://scut.itch.io/7drl-tileset-2018 (made by Scut, CC BY-NC 4.0)

https://creativecommons.org/publicdomain/zero/1.0/legalcode
https://creativecommons.org/licenses/by/4.0/legalcode
https://creativecommons.org/licenses/by-nc/4.0/legalcode
