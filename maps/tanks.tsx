<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="Tanks" tilewidth="32" tileheight="32" tilecount="4" columns="2">
 <image source="../assets/tanks.png" width="64" height="64"/>
 <tile id="0">
  <properties>
   <property name="player" value="1"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="player" value="2"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="player" value="3"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="player" value="4"/>
  </properties>
 </tile>
</tileset>
