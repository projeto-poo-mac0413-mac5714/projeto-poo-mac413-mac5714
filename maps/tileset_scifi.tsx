<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="scifi_resized" tilewidth="32" tileheight="32" tilecount="120" columns="8">
 <image source="../assets/tilesets/tileset_scifi.png" width="256" height="480"/>
 <tile id="3">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
</tileset>

