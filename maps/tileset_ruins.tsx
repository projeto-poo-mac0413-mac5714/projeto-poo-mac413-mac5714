<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="tileset_ruins" tilewidth="32" tileheight="32" tilecount="848" columns="8">
 <image source="../assets/tilesets/tileset_ruins.png" width="256" height="3392"/>
 <tile id="0">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="112">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="128">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="129">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="130">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="136">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="137">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="138">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="144">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="146">
  <properties>
   <property name="strategy" value="reflect"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="435">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="436">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="437">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="438">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="439">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="443">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="444">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="445">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="446">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="447">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="453">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="454">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="455">
  <properties>
   <property name="strategy" value="destroy"/>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="461">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="462">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="463">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
 <tile id="536">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="537">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="538">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="539">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="544">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="545">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="546">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="547">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="552">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="553">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="554">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="555">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="560">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="561">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="562">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="563">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="782">
  <properties>
   <property name="type" value="floor"/>
  </properties>
 </tile>
</tileset>
