## Membros:
### João Pevidor - 11221992
### Julia Leite - 11221797
### Luca Diogo - 12542693
### Luciano Leão - 11221817

## Descrição
Jogo de combate entre tanques baseado em Combat) e Tank 1990 (NES). O jogo consiste em um tanque controlável pelo jogador que atira projéteis e pode destruir o cenário da fase; seu objetivo consiste em derrotar os tanques inimigos que também estão no cenário.

## Fonte dos assets
- https://opengameart.org/content/tanks (Criado pelo usuário zironid_n, com licença CC0).
- https://opengameart.org/content/32x32-dungeon-tileset (Criado pelo usuário stealthix, com licença CC0)
- https://opengameart.org/content/explosion (Criado pelo usuário Cuzco, com licença CC0)
- https://opengameart.org/content/electrical-disintegration-animation (Criado pelo usuário superjoe, com licença CC0)
- https://opengameart.org/content/wrench-0 (Criado pelo usuário Santonich, com licença CC BY 4.0)
- https://opengameart.org/content/spaceship-bullet (Criado pelo usuário vergil1018, com licença CC0)
- https://opengameart.org/content/aztec-tileset (Criado pelo usuário Sevarihk, com licença CC BY 4.0)
- https://opengameart.org/content/poof-effect-spritesheet (Criado pelo usuário jellyfizh, com licença CC0)
- https://scut.itch.io/7drl-tileset-2018 (Criado pelo usuário Scut, com licença CC BY-NC 4.0)

## Instruções de compilação (para sistemas UNIX*)
### Instalar Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh    

### Instalar dependências
Para linux: https://github.com/not-fl3/macroquad/tree/master#linux 

### Instruções de execução
No diretório do projeto, rodar: cargo run.


## Fase 1:
- [X] Fazer uma engine simples implementado o padrão [ECS](https://en.wikipedia.org/wiki/Entity_component_system) para desacoplar comportamentos (de maneira parecida com a do padrão Strategy)
  - Na pasta core/ temos dois módulos:
    - Game: Contém a classe Game que representa o jogo, e define um interface para o game loop.
    - Simulation: implementação do padrão ECS.
  - Na pasta mq-impl/ implementamos uma infraestrutura para ser usada no jogo, para isso usamos a biblioteca (Macroquad)[https://github.com/not-fl3/macroquad/] junto com o padrão Adapter para que não acoplar o jogo com os detalhes de nível mais baixo, então, poderíamos trocar a biblioteca Macroquad por outro sem alterar no código do jogo propriamente dito:
    - mq-game: Implementa o gameloop e serve como Facade para as componentes definidas nas outras pastas.
    - input: conjunto de funções estáticas para capturar a entrada.
    - physics: Implementação de componentes que representam posição, rotação, velocidade, e caixa de colisão no plano.
    - render: Implementação de uma componente que representa uma imagem na janela.
  - A implementação do jogo propriamente dita está em src/ e os testes estão em src/test/
- [X] Saída Gráfica simples
- [X] Um tanque controlável
  - Setas para cima e para baixo movem o tanque, setas para esquerda e para direita rotacionam o tanque.

## Fase 2:
- [x] Tanque atira e recebe dano
  - [ ] ~~Usar o padrão Command para desacoplar a entrada com a ação de atirar e andar.~~ 
  - Implementamos a bala (src/bullet.rs) que pode ser disparada do jogador com a tecla espaço, também implementamos uma barra de vida para o jogador (src/healthbar.rs) que diminui quando uma bala o atinge.
- [x] Efeitos visuais
  - Adicionamos a animação de explosão quando a bala atinge o player e quando o jogador morre. Para isso, alteramos a componente Sprite (mq-impl/render/src/sprite.rs) de forma a cortar uma spritesheet e passar os frames com o tempo.  
  - Além disso, quando destruídos,  as balas, os jogadores e os blocos desaparecem.
- [x] Cenário: ambiente destruível
  -  Essa foi a parte que mais focamos nessa fase, para criar o mapa usamos o programa de software livre Tiled (https://www.mapeditor.org/) para gerar o arquivo maps/map.tmx. ![editor do Tiled](assets/tiled.png)
  - [X] Usar o padrão Abstract Factory para construir o mapa.
    - Criamos a interface MapFactory (src/map/factories/mod.rs) com o método parse() que dado um arquivo gera os layers do mapa. O mapa é construído com a função build_map() (src/map/factories/mod.rs) que recebe um argumento genérico que implementa a interface MapFactory (escolhemos implementar o padrão usando polimorfismo  em tempo de compilação, usando programação genérica).
    - Além disso, usamos uma estratégia parecida com o padrão Builder para construir o mapa, pois é necessário adicionar tanques, muros e pisos. Para isso criamos as funções build_tank(), build_wall() e build_floor() (src/map/factories/mod.rs) para serem usadas pela função build_map().
  - [x] Usar padrão Strategy para permitir que paredes ao tomarem tiros reflitam ou se quebrem.
    - Definimos a interface TileStrategy (src/map/tile_strategy/mod.rs) com o método on_collision() que lida com a colisão da bala com o bloco. A estratégia é a aplicada no método update() do Tile (src/map/tile.rs). Criamos duas estratégias:
      - ReflectStrategy (src/map/tile_strategy/reflect_strategy.rs): responsável por refletir a bala quando ele atinge o bloco.
      - DestroyStrategy (src/map/tile_strategy/destroy_strategy.rs): responsável por destruir o bloco quando a bala atinge o bloco certo número de vezes, além disso usamos o padrão Decorator para que seja possível aplicar outra estratégia simultaneamente (assim a bala também pode ser refletida depois que destrói o bloco).
- [X] Protótipo de partida e condição de vitória
  - [X] Numa partida, múltiplos tanques tentam se eliminar até sobrar um
  - Quando a vida do jogador chega a zero uma mensagem de game over surge na tela. Para isso, criamos a classe UI (src/ui.rs) que usa a classe Label (mq-impl/render/src/label.rs) para mostrar texto na tela.

- Além disso, refatoramos e melhoramos o módulo de física (mq-impl/physics) permitindo colisão de círculos e retângulos (sendo possível mostrar aa caixas de colisão com a flag debug do CollisionBox) e consertamos problemas com rotação e movimentação. Também adicionamos mais testes, principalmente testes do mapa.
- Também removemos por completo referências diretas à biblioteca Macroquad da lógica do jogo (src/), agora só há referência a ela nos módulos da pasta mq-impl/, assim separamos os detalhes das regras de negócio.

## Fase 3:
- [x] Comportamento dos tanques inimigos
  - Usar o padrão Command para desacoplar a entrada com a ação de atirar e andar. No caminho plugins/commands/ temos a interface para a implementação do padrão. Em src/commands/mod.rs
  temos a implementação do padrão para o jogo que inclui as funcionalidades:
    - Andar para frente (MoveForwardCommand)
    - Andar para trás  (MoveBackwardCommand)
    - Rotacionar para a esquerda (RotateLeftCommand)
    - Rotacionar para a direita  (RotateRightCommand) 
    - Atirar (ShootCommand). 
  Assim, podemos facilmente criar diferentes layouts para a realização dessas ações.
  
  - Além disso, em plugins/commands/src/factory.rs temos uma interface para a definição de uma fábrica que irá gerar os diferentes comandos, utilizando o padrão Abstract Factory. Em src/commands/factories temos três implementações concretas dessa fábrica, tal que uma delas cuida da criação de inputs para o jogo local e a outras utilizando o padrão Decorator, para adicionar o comportamento de enviar os comandos por rede ou gravar em disco em um arquivo de replay.
  - Adicionar tanques controláveis por múltiplos jogadores ou por IA.

- [x] Troca de cenários
  - ~~Usar o padrão Abstract Factory para criar diferentes cenários nas fases.~~ 
  - A criaçao de diferentes cenários foi implementada a partir do _parsing_ de arquivos gerados pelo programa Tiled. Com ele, geramos três arquivos diferentes descrevendo três mapas diferentes. 
  - O programa Tiled pode exportar diferentes tipos de arquivos para descrever o mapa. Para realizar o _parsing_ do arquivo, implementamos o padrão de Fábrica Abstrata (src/map/factories/), para permitir a extensão de uma nova classe para a adaptação a um tipo diferente de arquivo. Assim, a classe concreta (src/map/factories/xml.rs) e implementa um _parser_ para o formato XML que realiza o mapeamento entre a descrição do arquivo gerado e as classes implementadas no projeto.
  - Para a criação da representação em memória do mapa em tempo de execução, utilizamos o padrão Builder para criar uma classe que utilizando a classe do _parser_.

- [x] Poderes
    - Usar o padrão Decorator para implementar poderes para os tanques: tiro duplo, maior resistência, maior velocidade. Em (src/item/item_decorator.rs) Implementamos os diferentes componentes que irão realizar o comportamento de decoração assim que forem ativados, alterando o comportamento dos nossos jogadores.
    - Implementamos os itens para serem gerados pseudo-aleatoriamente no mapa e darem diferentes melhorias para os jogadores. Esse diferença de comportamento foi obtida a partir da implementação do padrão Strategy em src/item/item_strategy.rs. Os jogadores podem obter as seguintes melhorias:
      - HealingEffect: Cura o jogador;
      - SpeedEffect: Aumenta a velocidade do jogador;
      - SpecialBulletEffect: Aumenta o dano e a aparência da bala do jogador;
      - DummyEffect: Não realiza melhoria nenhuma, serve como isca para os jogadores se arriscarem por ele;
    - Todos os itens produzem efeitos especiais quando o jogador consome eles, sendo que o DummyEffect tem um efeito diferente para demonstrar que um item inútil foi adquirido.

- [x] Projeteis com diferentes comportamentos
  - ~~Usar o padrão Strategy para fazer balas explodirem ou congelarem os alvos.~~ Implementamos em src/bullet/bullet_strategy.rs estratégias diferentes para o comportamento da bala, já que ao adquirir um item de melhoria, queremos que ele tenha efeitos diferentes no jogo. O efeito implementado foi o de aplicar mais dano, acompanhado de um efeito visual diferente.

## Implementações adicionais
- [x] Multiplayer em rede
  - É possível executar uma partida de dois jogadores conectados por rede.

- [x] Sistema de gravação e execução de replays
  - É possível gravar os comandos executados numa partida em um arquivo e executar esse arquivo de forma a reproduzir exatamente o que aconteceu durante o jogo.

- [x] CI
  - Implementamos no gitlab uma bateria de testes automáticos executada a cada commit que era enviado para o repositório, que nos avisava por email quando os testes falham;

- [x] Deploy na web
  - O jogo pode ser acessado e jogado localmente em [Tanquinhos!](https://linux.ime.usp.br/~lucianoleao/tanquinhos/index.html)

- [x] Funcionalidades na linha comando
  - Ao executar ``cargo run -- --help``, o usuário pode obter uma descrição de como utilizar o programa, incluindo instruções de como iniciar uma partida online, como salvar o replay da partir e como executar esse replay.


