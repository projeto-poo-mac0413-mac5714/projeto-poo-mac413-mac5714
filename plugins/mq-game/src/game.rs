use game::prelude::*;
use macroquad::prelude::*;
use simulation::prelude::Simulation;

struct MacroquadGameLoop {
    name: &'static str,
    _icon_path: &'static str,
    width: i32,
    height: i32,
}

impl GameLoop for MacroquadGameLoop {
    fn run(&mut self, simulation: Simulation) {
        macroquad::Window::from_config(
            Conf {
                window_title: self.name.into(),
                window_width: self.width,
                window_height: self.height,
                high_dpi: false,
                window_resizable: true,
                icon: None,
                ..Default::default()
            },
            MacroquadGameLoop::game_loop(simulation),
        );
    }
}

impl MacroquadGameLoop {
    async fn game_loop(mut simulation: Simulation) {
        simulation = simulation.init().await;

        loop {
            clear_background(BLACK);
            simulation = simulation.update().await;
            next_frame().await
        }
    }

    fn new(name: &'static str, icon_path: &'static str, width: i32, height: i32) -> Self {
        Self {
            name,
            _icon_path: icon_path,
            width,
            height,
        }
    }
}

pub fn new_game_window(
    name: &'static str,
    icon_path: &'static str,
    width: i32,
    height: i32,
    simulation: Simulation,
) -> Game {
    Game::new(
        simulation,
        Box::new(MacroquadGameLoop::new(name, icon_path, width, height)),
    )
}

pub fn new_game(name: &'static str, simulation: Simulation) -> Game {
    Game::new(
        simulation,
        Box::new(MacroquadGameLoop::new(name, "", 480, 360)),
    )
}
