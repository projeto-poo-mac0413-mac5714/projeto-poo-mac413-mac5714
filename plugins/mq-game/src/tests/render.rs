use crate::game::*;
use game::prelude::*;
use render::*;

#[test]
fn render_sprite() {
    let mut simulation = Simulation::new();

    let mut entity = simulation.new_entity();
    let sprite = Sprite::new("../../assets/sprite.png".to_string());

    entity.add_component(sprite).unwrap();

    simulation.add_entity(entity).unwrap();

    new_game("Sprite Test", simulation).run();
}
