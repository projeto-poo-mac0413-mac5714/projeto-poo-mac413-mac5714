pub(crate) mod game;

#[cfg(test)]
mod tests;

pub mod prelude {
    pub use crate::game::*;
    pub use game::prelude::*;
    pub use input::*;
    pub use macroquad::prelude::debug;
    pub use macroquad::prelude::load_file;
    pub use net::*;
    pub use physics::*;
    pub use render::*;
}
