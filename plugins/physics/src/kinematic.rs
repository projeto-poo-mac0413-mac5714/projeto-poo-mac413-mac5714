use std::io;

use macroquad::prelude::Vec2;
use simulation::prelude::{async_trait, Component, Entity, Simulation};

use crate::{CollisionManager, Transform};

pub const DELTA: f32 = 0.001;

#[derive(Default, Clone, Copy)]
pub enum CollisionMode {
    #[default]
    Stop,
    Reflect,
}

#[derive(Default)]
pub struct Kinematic {
    pub velocity: Vec2,
    pub angular_velocity: f32,
    pub acceleration: Vec2,
    pub angular_acceleration: f32,
    pub collision_mode: CollisionMode,
}

impl Kinematic {
    pub fn new() -> Kinematic {
        Self::default()
    }

    pub fn set_collision_mode(mut self, collision_mode: CollisionMode) -> Self {
        self.collision_mode = collision_mode;
        self
    }
}

#[async_trait(?Send)]
impl Component for Kinematic {
    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let transform = entity.get_component::<Transform>().unwrap();
        let mut to_transform = transform.clone();

        let position = transform.position;
        let rotation = transform.rotation;

        to_transform.rotation = rotation + DELTA * self.angular_velocity;
        to_transform.position = position + DELTA * self.velocity;

        if CollisionManager::try_move(entity, to_transform, simulation).is_err() {
            match self.collision_mode {
                CollisionMode::Stop => {
                    self.angular_velocity = 0.;
                    self.velocity = Vec2::ZERO;
                }
                CollisionMode::Reflect => {
                    let transform = entity.get_component::<Transform>().unwrap();
                    self.velocity += self.acceleration;
                    self.angular_velocity += self.angular_acceleration;
                    *transform = to_transform;
                }
            }
        } else {
            self.velocity += self.acceleration;
            self.angular_velocity += self.angular_acceleration;

            let transform = entity.get_component::<Transform>().unwrap();
            *transform = to_transform;
        }
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }
}
