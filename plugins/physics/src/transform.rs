use macroquad::prelude::Vec2;
use simulation::prelude::Component;

#[derive(Default, Clone, Copy)]
pub struct Transform {
    pub position: Vec2,
    pub scale: Vec2,
    pub rotation: f32,
}

impl Transform {
    pub fn new() -> Transform {
        Transform {
            position: Vec2::ZERO,
            scale: Vec2::ONE,
            rotation: 0.,
        }
    }
}

impl Component for Transform {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }
}
