use macroquad::{
    prelude::{Color, Vec2},
    shapes::{draw_circle, draw_rectangle_ex, DrawRectangleParams},
};
use simulation::prelude::{async_trait, Component, Entity, EntityID};
use std::{any::Any, io};

use crate::Transform;

const COLLISION_COLOR: Color = Color::new(0.0, 0.7, 1.0, 0.5);

pub struct CollisionBox {
    pub shape: CollisionShape,
    pub layers: u8,
    pub masks: u8,
    pub area_masks: u8,
    pub debug: bool,
    pub(super) colliders: Vec<EntityID>,
}

pub enum CollisionShape {
    Circle { radius: f32 },
    Rectangle { width: f32, height: f32 },
}

impl CollisionBox {
    pub fn new(shape: CollisionShape, layers: u8, masks: u8) -> CollisionBox {
        CollisionBox {
            shape,
            layers,
            masks,
            area_masks: 0,
            debug: false,
            colliders: Vec::new(),
        }
    }

    pub fn get_colliders(&self) -> Vec<EntityID> {
        self.colliders.clone()
    }

    pub fn set_area_masks(mut self, masks: u8) -> Self {
        self.area_masks = masks;
        self
    }
}

#[async_trait(?Send)]
impl Component for CollisionBox {
    fn as_any(&mut self) -> &mut dyn Any {
        self
    }

    async fn update(
        &mut self,
        entity: &mut Entity,
        _simulation: &mut simulation::prelude::Simulation,
    ) -> io::Result<()> {
        let transform = entity.get_component::<Transform>().unwrap().clone();

        if self.debug {
            self.shape.debug_draw(transform);
        }

        Ok(())
    }

    fn sequence(&self) -> i32 {
        10
    }
}

impl CollisionShape {
    fn debug_draw(&self, transform: Transform) {
        match &self {
            CollisionShape::Circle { radius } => draw_circle(
                transform.position.x,
                transform.position.y,
                *radius,
                COLLISION_COLOR,
            ),
            CollisionShape::Rectangle { width, height } => draw_rectangle_ex(
                transform.position.x - *width / 2.,
                transform.position.y - *height / 2.,
                *width,
                *height,
                DrawRectangleParams {
                    offset: Vec2::ZERO,
                    rotation: 0.,
                    color: COLLISION_COLOR,
                },
            ),
        }
    }
}
