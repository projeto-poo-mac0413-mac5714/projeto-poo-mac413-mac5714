use std::io;

use simulation::prelude::{async_trait, Component, Entity, EntityID, Simulation};

use crate::{CollisionBox, CollisionShape, Kinematic, Transform, DELTA};

#[derive(Default)]
pub struct CollisionManager;

impl CollisionManager {
    pub fn new_entity(simulation: &mut Simulation) -> Entity {
        let mut entity = simulation.new_entity();
        entity.add_component(CollisionManager).unwrap();
        entity
    }

    fn get_colliders(who: &mut Entity, simulation: &mut Simulation) -> Vec<EntityID> {
        let mut entities = simulation.query::<CollisionBox>();

        let entity_1 = who;

        let transform_1 = entity_1.get_component::<Transform>().unwrap().clone();
        let box_1 = entity_1.get_component::<CollisionBox>().unwrap();

        let mut colliders = Vec::new();

        for entity_2 in &mut entities {
            let entity_2_id = entity_2.get_id();
            let transform_2 = entity_2.get_component::<Transform>().unwrap().clone();
            let box_2 = entity_2.get_component::<CollisionBox>().unwrap();

            if box_1.masks & box_2.layers != 0
                && check_collision(&box_1.shape, &box_2.shape, &transform_1, &transform_2)
            {
                colliders.push(entity_2_id);
            }
        }

        colliders
    }

    pub fn try_move(
        who: &mut Entity,
        to: Transform,
        simulation: &mut Simulation,
    ) -> Result<(), ()> {
        let transform = who.get_component::<Transform>().unwrap();
        let previous_transform = transform.clone();

        *transform = to.clone();

        if who.get_component::<CollisionBox>().is_err() {
            return Ok(());
        }

        if !Self::get_colliders(who, simulation).is_empty() {
            let transform = who.get_component::<Transform>().unwrap();
            *transform = previous_transform.clone();

            return Err(());
        }

        Ok(())
    }

    pub fn get_future_colliders(who: &mut Entity, simulation: &mut Simulation) -> Vec<EntityID> {
        let to = who.get_component::<Kinematic>().unwrap().velocity.clone();
        let transform = who.get_component::<Transform>().unwrap();
        let to2 = transform.position + to * DELTA;
        let mut to = transform.clone();
        to.position = to2;
        let previous_transform = transform.clone();

        *transform = to.clone();

        if who.get_component::<CollisionBox>().is_err() {
            return Vec::new();
        }

        let colliders = Self::get_colliders(who, simulation);

        let transform = who.get_component::<Transform>().unwrap();
        *transform = previous_transform.clone();

        colliders
    }

    pub fn register(simulation: &mut Simulation) -> Result<(), &str> {
        let collision_manager = CollisionManager::new_entity(simulation);
        simulation.add_entity(collision_manager)
    }
}

#[async_trait(?Send)]
impl Component for CollisionManager {
    async fn update(
        &mut self,
        _entity: &mut Entity,
        simulation: &mut Simulation,
    ) -> io::Result<()> {
        let mut entities = simulation.query::<CollisionBox>();

        for _ in 0..entities.len() {
            let entity_1 = entities.pop_front().unwrap();
            // let id1 = entity_1.get_id();
            let transform_1 = entity_1.get_component::<Transform>().unwrap().clone();
            let box_1 = entity_1.get_component::<CollisionBox>().unwrap();

            box_1.colliders = Vec::new();

            for entity_2 in &mut entities {
                // let id2 = entity_2.get_id();
                let transform_2 = entity_2.get_component::<Transform>().unwrap().clone();
                let box_2 = entity_2.get_component::<CollisionBox>().unwrap();

                if (box_1.area_masks & box_2.layers) != 0
                    && check_collision(&box_1.shape, &box_2.shape, &transform_1, &transform_2)
                {
                    box_1.colliders.push(entity_2.get_id());
                }
            }

            entities.push_back(entity_1);
        }

        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    fn sequence(&self) -> i32 {
        -1
    }
}

fn check_collision(
    box_1: &CollisionShape,
    box_2: &CollisionShape,
    transform_1: &Transform,
    transform_2: &Transform,
) -> bool {
    match box_1 {
        CollisionShape::Circle { radius: radius_1 } => match box_2 {
            CollisionShape::Circle { radius: radius_2 } => {
                check_collision_circle_circle(*radius_1, *radius_2, transform_1, transform_2)
            }
            CollisionShape::Rectangle { width, height } => check_collision_rectangle_circle(
                *width,
                *height,
                *radius_1,
                transform_2,
                transform_1,
            ),
        },
        CollisionShape::Rectangle { width, height } => match box_2 {
            CollisionShape::Circle { radius } => {
                check_collision_rectangle_circle(*width, *height, *radius, transform_1, transform_2)
            }
            CollisionShape::Rectangle {
                width: _,
                height: _,
            } => {
                panic!("RECTANGLE-RECTANGLE collision not implemented yet!")
            }
        },
    }
}

fn check_collision_circle_circle(
    radius_1: f32,
    radius_2: f32,
    transform_1: &Transform,
    transform_2: &Transform,
) -> bool {
    transform_1.position.distance(transform_2.position) <= radius_1 + radius_2
}

fn check_collision_rectangle_circle(
    width_1: f32,
    height_1: f32,
    radius_2: f32,
    transform_1: &Transform,
    transform_2: &Transform,
) -> bool {
    let relative = (transform_2.position) - (transform_1.position);
    let width = width_1 / 2. + radius_2;
    let height = height_1 / 2. + radius_2;

    (-width <= relative.x && relative.x <= width) && (-height <= relative.y && relative.y <= height)
}
