use std::{
    collections::{HashMap, VecDeque},
    io::{self, Read, Write},
    net::{TcpListener, TcpStream},
    thread::sleep,
    time::Duration,
};

use game::prelude::*;

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum PeerType {
    Sender,
    Receiver,
}

pub type PeerID = u32;
const FRAME_BUFFER_SIZE: u32 = 2;

#[derive(Clone, Default)]
pub struct Packet {
    // pub frame: u32,
    pub payload: Vec<u8>,
}
pub struct Peer {
    frame: u32,
    buffer: Vec<u8>,
    packets: VecDeque<Packet>,
    peer_type: PeerType,
    id_to_socket: HashMap<u8, TcpStream>,
}

#[derive(Default)]
pub struct NetworkManager {
    frame: u32,
    peers: HashMap<PeerID, Peer>,
    available_id: PeerID,
}

impl NetworkManager {
    pub fn host(&mut self, port: u32, peer_type: PeerType) -> PeerID {
        let id = self.available_id;

        self.available_id += 1;
        self.peers.insert(id, Peer::host(port, peer_type));

        id
    }

    pub fn connect(&mut self, addr: String, port: u32, peer_type: PeerType) -> PeerID {
        let id = self.available_id;

        self.available_id += 1;
        self.peers.insert(id, Peer::connect(addr, port, peer_type));

        id
    }

    pub fn peer(&mut self, id: PeerID) -> Option<&mut Peer> {
        self.peers.get_mut(&id)
    }

    pub fn new_entity(simulation: &mut Simulation) -> Entity {
        let mut entity = simulation.new_entity().set_sequence(-1);
        entity
            .add_component(NetworkManager {
                available_id: 1,
                frame: 1,
                ..Default::default()
            })
            .unwrap();

        entity.set_sequence(11)
    }
}

impl Peer {
    fn host(port: u32, peer_type: PeerType) -> Self {
        let listener = TcpListener::bind(format!("0.0.0.0:{port}")).unwrap();

        let (socket, _) = listener.accept().unwrap();

        socket.set_nonblocking(true).unwrap();
        socket.set_nodelay(true).unwrap();

        let mut id_to_socket = HashMap::new();

        id_to_socket.insert(2, socket);

        let packets = VecDeque::from(vec![Packet::default(); 2 * FRAME_BUFFER_SIZE as usize]);

        Self {
            peer_type,
            id_to_socket,
            packets,
            buffer: vec![],
            frame: 1,
        }
    }

    fn connect(addr: String, port: u32, peer_type: PeerType) -> Self {
        let socket = loop {
            let s = TcpStream::connect(format!("{addr}:{port}"));
            if s.is_err() {
                sleep(Duration::new(0, 10_000_000));
            } else {
                break s;
            }
        }
        .unwrap();

        socket.set_nonblocking(true).unwrap();
        socket.set_nodelay(true).unwrap();

        let mut id_to_socket = HashMap::new();

        id_to_socket.insert(1, socket);

        let packets = VecDeque::from(vec![Packet::default(); 2 * FRAME_BUFFER_SIZE as usize]);

        Self {
            peer_type,
            id_to_socket,
            packets,
            buffer: vec![],
            frame: 1,
        }
    }

    pub fn push_packet(&mut self, msg: Vec<u8>) {
        let packet = Packet { payload: msg };
        self.packets.push_back(packet.clone());

        let mut msg = vec![];
        msg.append(&mut packet.clone().payload);

        for socket in self.id_to_socket.values_mut() {
            let _ = socket.write(&msg).unwrap();
        }
        self.frame += 1;
    }

    fn receive_packets(&mut self) {
        for socket in self.id_to_socket.values_mut() {
            loop {
                let mut buf = [0; 255];
                match socket.read(&mut buf) {
                    Ok(0) => {
                        break;
                    }
                    Ok(n) => {
                        self.buffer.append(&mut Vec::from(&buf[..n]));
                    }
                    Err(_) => {
                        break;
                    }
                }
            }
        }

        let mut buffers: Vec<Vec<u8>> = self
            .buffer
            .split(|b| *b == 0)
            .map(|x| Vec::from(x))
            .collect();

        self.buffer = buffers.pop().unwrap_or(vec![]);

        for buffer in buffers {
            self.frame += 1;
            self.packets.push_back(Packet { payload: buffer });
        }
    }

    pub fn pop_packet(&mut self) -> Option<Packet> {
        self.packets.pop_front()
    }

    pub fn get_frame(&self) -> u32 {
        self.frame
    }

    pub fn close(&mut self) {
        for socket in self.id_to_socket.values_mut() {
            let mut _buf = vec![];
            socket.read_to_end(&mut _buf).unwrap();
        }
    }
}

#[async_trait(?Send)]
impl Component for NetworkManager {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        simulation: &mut Simulation,
    ) -> io::Result<()> {
        let mut to_pause = false;

        for peer in self.peers.values_mut() {
            peer.receive_packets();

            if peer.peer_type != PeerType::Sender && (peer.packets.len() as u32) < FRAME_BUFFER_SIZE
            {
                to_pause = true;
            }
        }

        if to_pause {
            simulation.set_paused(true);
        } else {
            simulation.set_paused(false);
            self.frame += 1;
        }

        Ok(())
    }

    fn pauseable(&self) -> bool {
        false
    }

    fn sequence(&self) -> i32 {
        1
    }
}
