use macroquad::prelude::{get_last_key_pressed, is_key_down, is_key_pressed, is_key_released};

use crate::KeyCode;

// accel and ang accel suffer exp decay
pub const DECAY: f32 = 0.01;
pub struct Input {}

impl Input {
    /// gets the last key that was pressed
    pub fn get_last_key_pressed() -> Option<KeyCode> {
        get_last_key_pressed().map(|x| KeyCode::from(x))
    }

    /// checks if the keys is pressed right now
    pub fn is_key_down(key_code: KeyCode) -> bool {
        is_key_down(key_code.into())
    }

    /// checks if the key has just been pressed
    pub fn is_key_just_pressed(key_code: KeyCode) -> bool {
        is_key_pressed(key_code.into())
    }

    /// checks if the key has been released
    pub fn is_key_released(key_code: KeyCode) -> bool {
        is_key_released(key_code.into())
    }
}
