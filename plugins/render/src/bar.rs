use std::io;

use macroquad::prelude::*;
use physics::Transform;
use simulation::prelude::*;

pub struct Bar {
    pub value: f32,
}

impl Bar {
    pub fn new() -> Bar {
        Bar { value: 100.0 }
    }

    pub fn draw(&self, x: f32, y: f32, w: f32, h: f32) {
        let spacing = 2.0;
        // draw Bar background in BLACK
        draw_rectangle(x, y - h - spacing, w, h, BLACK);

        let thickness = 5.0;
        let thick_delta = thickness / 2.0;

        let inner_w = w - thickness;
        let relative_w = (inner_w * self.value) / 100.0;
        let inner_h = h - thickness;

        // draw Bar foreground (actual health) in RED
        draw_rectangle(
            x + thick_delta,
            y - h - spacing + thick_delta,
            relative_w,
            inner_h,
            RED,
        );
    }
}

#[async_trait(?Send)]
impl Component for Bar {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        let transform = *entity.get_component::<Transform>().unwrap();
        let pos = transform.position;

        let w = 50.0;
        let h = 15.0;
        self.draw(
            pos.x - transform.scale.x / 2.,
            pos.y - transform.scale.y / 2.,
            w,
            h,
        );
        Ok(())
    }

    fn sequence(&self) -> i32 {
        9
    }
}
