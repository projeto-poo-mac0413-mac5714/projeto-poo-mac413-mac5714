use std::io;

use macroquad::prelude::*;
use physics::Vec2;
use simulation::prelude::*;

pub struct Label {
    pub text: String,
    pub visible: bool,
    pub position: Vec2,
    pub font_size: f32,
    pub color: Color,
}

impl Label {
    fn draw_text(&self, _simulation: &mut Simulation) {
        draw_text(
            self.text.as_str(),
            self.position.x,
            self.position.y,
            self.font_size,
            self.color,
        );
    }
}

#[async_trait(?Send)]
impl Component for Label {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        simulation: &mut Simulation,
    ) -> io::Result<()> {
        if self.visible {
            self.draw_text(simulation);
        }
        Ok(())
    }

    fn sequence(&self) -> i32 {
        100
    }
}
