use std::{collections::HashMap, io};

use macroquad::texture::{load_texture, Texture2D};
use simulation::prelude::*;

#[derive(Default)]
pub struct TextureManager {
    textures: HashMap<String, Option<Texture2D>>,
}

impl TextureManager {
    pub async fn load_texture(&mut self, path: String) -> Option<Texture2D> {
        if let Some(texture) = self.textures.get(&path) {
            return texture.as_ref().map(|t| t.clone());
        }

        let texture = Some(load_texture(path.as_str()).await.unwrap());

        self.textures
            .insert(path, texture.as_ref().map(|x| x.clone()));

        texture
    }

    pub fn register(simulation: &mut Simulation) -> Result<(), &str> {
        let manager = TextureManager::default();
        simulation.register_resource(manager)
    }
}

#[async_trait(?Send)]
impl Component for TextureManager {
    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        Ok(())
    }

    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    fn sequence(&self) -> i32 {
        0
    }

    fn pauseable(&self) -> bool {
        false
    }
}
