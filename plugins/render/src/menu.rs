use macroquad::color::{colors, Color};
use macroquad::prelude::{clear_background, RectOffset, Vec2};
use macroquad::{hash, ui, window::next_frame};
use simulation::prelude::{async_trait, Component, Entity, Simulation};
use std::io;

pub struct Menu {
    pub map_selected: &'static str,
    pub trying_select: bool,
    pub selected: bool,
    pub quit_screen: bool,
    pub start: bool,
}

impl Menu {
    pub fn new() -> Menu {
        Menu {
            map_selected: "",
            trying_select: false,
            selected: false,
            quit_screen: false,
            start: true,
        }
    }

    async fn show_start_screen(&mut self) {
        start_screen(self).await;
    }
}

#[async_trait(?Send)]
impl Component for Menu {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        if self.start {
            self.show_start_screen().await;
        }

        Ok(())
    }

    fn sequence(&self) -> i32 {
        100
    }
}

async fn start_screen(menu: &mut Menu) {
    let skin = {
        let label_style = ui::root_ui()
            .style_builder()
            .text_color(Color::from_rgba(180, 180, 120, 255))
            .font_size(50)
            .build();

        let window_style = ui::root_ui()
            .style_builder()
            .background_margin(RectOffset::new(20.0, 20.0, 10.0, 10.0))
            .margin(RectOffset::new(-20.0, -30.0, 0.0, 0.0))
            .build();

        let button_style = ui::root_ui()
            .style_builder()
            .background_margin(RectOffset::new(37.0, 37.0, 5.0, 5.0))
            .margin(RectOffset::new(10.0, 10.0, 0.0, 0.0))
            .text_color(Color::from_rgba(180, 180, 100, 255))
            .font_size(40)
            .build();

        ui::Skin {
            window_style,
            button_style,
            label_style,
            ..ui::root_ui().default_skin()
        }
    };

    let window_skin = skin.clone();
    let mut map_selected = "";
    ui::root_ui().push_skin(&window_skin);

    loop {
        clear_background(colors::GRAY);

        ui::root_ui().window(
            hash!(),
            Vec2 { x: 170., y: 185. },
            Vec2 { x: 400., y: 400. },
            |ui| {
                ui::widgets::Label::new("Level Selection")
                    .position(Vec2 { x: 40.0, y: 15.0 })
                    .ui(ui);

                let dungeon_but = ui::widgets::Button::new("Dungeon")
                    .position(Vec2 { x: 90.0, y: 110.0 })
                    .ui(ui);
                let future_but = ui::widgets::Button::new("Futuristic")
                    .position(Vec2 { x: 65.0, y: 160.0 })
                    .ui(ui);
                let ruins_but = ui::widgets::Button::new("Ruins")
                    .position(Vec2 { x: 105.0, y: 210.0 })
                    .ui(ui);

                let quit_but = ui::widgets::Button::new("Quit")
                    .position(Vec2 { x: 110.0, y: 300.0 })
                    .ui(ui);

                if dungeon_but {
                    map_selected = "maps/map_dungeon.tmx";
                    menu.trying_select = true;
                }
                if future_but {
                    map_selected = "maps/map_scifi.tmx";
                    menu.trying_select = true;
                }
                if ruins_but {
                    map_selected = "maps/map_ruins.tmx";
                    menu.trying_select = true;
                }

                if quit_but {
                    menu.quit_screen = true;
                }
            },
        );

        if menu.trying_select == true {
            clear_background(colors::GRAY);

            ui::root_ui().window(
                hash!(),
                Vec2 { x: 220., y: 235. },
                Vec2 { x: 300., y: 300. },
                |ui| {
                    ui::widgets::Label::new("Confirm?")
                        .position(Vec2 { x: 45.0, y: 15.0 })
                        .ui(ui);

                    let yes = ui::widgets::Button::new("Yes")
                        .position(Vec2 { x: 40.0, y: 100.0 })
                        .ui(ui);
                    let no = ui::widgets::Button::new("No")
                        .position(Vec2 { x: 45.0, y: 160.0 })
                        .ui(ui);

                    if yes {
                        menu.map_selected = map_selected;
                        menu.quit_screen = true;
                        menu.selected = true;
                    }

                    if no {
                        menu.trying_select = false;
                    }
                },
            );
        }

        if menu.quit_screen == true {
            break;
        }
        next_frame().await;
    }
    menu.start = false
}
