use std::io;

use macroquad::prelude::*;
use physics::Transform;
use simulation::prelude::{async_trait, Component, Entity, Simulation};

use crate::texture_manager::TextureManager;

pub enum SpriteParameter {
    PathToSprite(String),
    LoadedSprite(Sprite),
}

pub struct AnimatedSprite {
    frame_rate: f32,
    repeat: bool,
}

#[derive(Default, Clone)]
pub struct Sprite {
    texture: Option<Texture2D>,
    path: String,
    layer: i32,
    pub frame: i32,
    hcells: i32,
    vcells: i32,
    reverse_frames: bool,
}

impl Sprite {
    pub fn new(path: String) -> Sprite {
        Sprite {
            texture: None,
            path,
            layer: 0,
            frame: 0,
            hcells: 1,
            vcells: 1,
            reverse_frames: false,
        }
    }

    // pub fn set_texture(mut self, texture: Option<Texture2D>) -> Self {
    //     self.texture = texture;
    //     self
    // }

    pub fn set_layer(mut self, layer: i32) -> Self {
        self.layer = layer;
        self
    }

    pub fn set_frame(mut self, frame: i32) -> Self {
        self.frame = frame;
        self
    }

    pub fn set_reverse_frames(mut self, reverse_frames: bool) -> Self {
        self.reverse_frames = reverse_frames;
        self
    }

    pub fn get_img_coords_from_frame(&self, height: f32, width: f32) -> Vec2 {
        if self.reverse_frames == true {
            return Vec2 {
                x: (self.hcells - self.frame % self.hcells - 1) as f32 * height,
                y: (self.vcells - self.frame / self.hcells - 1) as f32 * width,
            };
        }

        return Vec2 {
            x: (self.frame % self.hcells) as f32 * height,
            y: (self.frame / self.hcells) as f32 * width,
        };
    }

    pub fn crop(mut self, hcells: i32, vcells: i32) -> Self {
        self.hcells = hcells;
        self.vcells = vcells;
        self
    }
}

#[async_trait(?Send)]
impl Component for Sprite {
    async fn update(&mut self, entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        let transform = entity.get_component::<Transform>().unwrap();

        let _pos: Vec2 = transform.position;
        let _rot: f32 = transform.rotation;
        let _scale: Vec2 = transform.scale;

        if self.texture.is_none() {
            self.texture = simulation
                .get_resource::<TextureManager>()
                .unwrap()
                .load_texture(self.path.clone())
                .await;
        }

        let Some(_texture) = self.texture.as_ref() else {
            return Ok(());
        };

        let height = _texture.height() / (self.vcells as f32);
        let width = _texture.width() / (self.hcells as f32);

        let coords = self.get_img_coords_from_frame(height, width);

        draw_texture_ex(
            _texture,
            _pos.x - _scale.x / 2.,
            _pos.y - _scale.y / 2.,
            WHITE,
            DrawTextureParams {
                dest_size: (Some(_scale)),
                source: (Some(Rect {
                    x: coords.x,
                    y: coords.y,
                    w: width,
                    h: height,
                })),
                rotation: (_rot),
                flip_x: (false),
                flip_y: (false),
                pivot: (Some(_pos)),
            },
        );

        Ok(())
    }

    async fn init(&mut self, _entity: &mut Entity, simulation: &mut Simulation) -> io::Result<()> {
        if self.texture.is_none() {
            self.texture = simulation
                .get_resource::<TextureManager>()
                .unwrap()
                .load_texture(self.path.clone())
                .await;
        }

        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    fn sequence(&self) -> i32 {
        self.layer
    }

    fn pauseable(&self) -> bool {
        false
    }
}

impl AnimatedSprite {
    pub fn new() -> AnimatedSprite {
        AnimatedSprite {
            frame_rate: 1.,
            repeat: true,
        }
    }

    pub fn set_frame_rate(mut self, frame_rate: f32) -> Self {
        self.frame_rate = frame_rate;
        self
    }

    pub fn repeat(mut self, repeat: bool) -> Self {
        self.repeat = repeat;
        self
    }
}
#[async_trait(?Send)]
impl Component for AnimatedSprite {
    async fn update(
        &mut self,
        entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        let sprite = entity.get_component::<Sprite>().unwrap();

        let frame = (sprite.frame as f32 * self.frame_rate) as i32;
        let mut remove = false;

        if frame >= sprite.hcells * sprite.vcells {
            if self.repeat {
                sprite.frame = 0;
            } else {
                remove = true;
            }
        }
        sprite.frame += 1;
        if remove == true {
            _simulation.enqueue_remove(entity.get_id());
        }

        Ok(())
    }

    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }
}
