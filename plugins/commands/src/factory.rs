use simulation::prelude::*;

use crate::Command;

#[async_trait(?Send)]
pub trait CommandFactory {
    async fn generate_commands(&mut self, _simulation: &mut Simulation) -> Vec<Command>;
}
