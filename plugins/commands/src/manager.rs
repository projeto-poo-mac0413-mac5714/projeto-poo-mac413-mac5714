use std::{
    any::{Any, TypeId},
    collections::HashMap,
    io,
};

use simulation::prelude::*;

use crate::*;

pub struct CommandManager {
    commands: HashMap<CommandID, Box<dyn CommandTrait>>,
    type_id_to_command_id: HashMap<TypeId, CommandID>,
    available_id: CommandID,
}

impl CommandManager {
    pub fn register_command(
        &mut self,
        command: impl CommandTrait + Any + 'static,
    ) -> Result<(), &str> {
        if self.type_id_to_command_id.contains_key(&command.type_id()) {
            return Err("Command already registered!");
        }
        let type_id = command.type_id();

        let _ = self.commands.insert(self.available_id, Box::new(command));

        self.type_id_to_command_id
            .insert(type_id, self.available_id);

        self.available_id += 1;

        Ok(())
    }

    pub fn command_from_id(&self, id: CommandID) -> Option<Command> {
        match self.commands.get(&id) {
            Some(c) => Some(Command {
                id,
                command: c.as_ref().clone(),
            }),
            None => None,
        }
    }

    pub fn register(simulation: &mut Simulation) -> Result<(), &str> {
        let manager = CommandManager {
            commands: HashMap::new(),
            available_id: 1,
            type_id_to_command_id: HashMap::new(),
        };

        simulation.register_resource(manager)
    }

    pub fn new_command(&self, command: impl CommandTrait + Any) -> Result<Command, &str> {
        let Some(command_id) = self.type_id_to_command_id.get(&command.type_id()) else {
            return Err("Command not registered!");
        };

        Ok(Command {
            id: *command_id,
            command: self.commands.get(command_id).unwrap().as_ref().clone(),
        })
    }
}

#[async_trait(?Send)]
impl Component for CommandManager {
    async fn init(&mut self, _entity: &mut Entity, _simulation: &mut Simulation) -> io::Result<()> {
        Ok(())
    }

    // Ignore this
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    async fn update(
        &mut self,
        _entity: &mut Entity,
        _simulation: &mut Simulation,
    ) -> io::Result<()> {
        Ok(())
    }
}
