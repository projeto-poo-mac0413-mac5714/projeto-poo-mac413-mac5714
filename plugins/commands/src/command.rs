use simulation::prelude::*;

pub type CommandID = u8;

pub struct Command {
    pub(crate) id: CommandID,
    pub(crate) command: Box<dyn CommandTrait>,
}

impl Clone for Command {
    fn clone(&self) -> Self {
        Self {
            id: self.id.clone(),
            command: self.command.clone(),
        }
    }
}

pub trait CommandTrait {
    fn execute(&self, entity: &mut Entity, simulation: &mut Simulation);

    fn clone(&self) -> Box<dyn CommandTrait>;
}

impl Command {
    pub fn get_id(&self) -> CommandID {
        self.id
    }

    pub fn execute(&self, entity: &mut Entity, simulation: &mut Simulation) {
        self.command.execute(entity, simulation)
    }
}
